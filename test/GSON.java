
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.List;
import scribe.thrift.LogEntry;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nhantt4
 */
public class GSON {

    public static final GSON Instance = new GSON();
    public final Gson gson;
    
    Type type = new TypeToken<List<LogEntry>>() {
    }.getType();

    public GSON() {
        gson = new Gson();
    }

    public List<LogEntry> fromJSON(String jsonData) {
        return gson.fromJson(jsonData, type);
    }
    
    public static GSON getInstance() {
        return Instance;
    }

    
    
}