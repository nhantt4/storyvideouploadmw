package com.vng.zing.storyvideouploadmw.app;

import com.vng.zing.storyvideouploadmw.model.TFileUploadModel;
import com.vng.zing.storyvideouploadmw.servers.TServers;

/**
 *
 * @author namnq
 */
public class MainApp {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		
		// init
		TFileUploadModel.init();

		TServers tServers = new TServers();
		if (!tServers.setupAndStart()) {
			System.err.println("Could not start thrift servers! Exit now.");
			System.exit(1);
		}
	}
}
