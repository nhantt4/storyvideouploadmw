package com.vng.zing.storyvideouploadmw.model;

import com.vng.zing.common.IZStructor;
import com.vng.zing.common.ZErrorHelper;
import com.vng.zing.configer.ZConfig;
import com.vng.zing.idzen.thrift.wrapper.IdZenClient;
import com.vng.zing.jni.zcommonx.wrapper.ZCommonX;
import com.vng.zing.jni.zicache.ZiCache;
import com.vng.zing.storyvideouploadmw.thrift.wrapper.JZSMetaClient;
import com.vng.zing.list64.thrift.AddWay;
import com.vng.zing.list64.thrift.DupBehav;
import com.vng.zing.list64.thrift.FindMed;
import com.vng.zing.list64.thrift.PutEntryOption;
import com.vng.zing.list64.thrift.wrapper.List64Client;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.stats.Profiler;
import com.vng.zing.stats.ThreadProfiler;
import com.vng.zing.strcounter64.thrift.wrapper.StrCounter64Client;
import com.vng.zing.strliststr.thrift.wrapper.StrListStrClient;
import com.vng.zing.wte.extlibs.zaloprofile.ZaloProfileModelWithMCZiCache;
import com.vng.zing.storyvideouploadmw.common.ServerFarm;
import com.vng.zing.storyvideouploadmw.common.Util;
import com.vng.zing.storyvideouploadmw.common.ZiDBCoreClient;
import com.vng.zing.zapfileuploadmw.thrift.EQualityType;
import com.vng.zing.zapfileuploadmw.thrift.EUploadError;
import com.vng.zing.zapfileuploadmw.thrift.EUploadType;
import com.vng.zing.zapfileuploadmw.thrift.TChunkData;
import com.vng.zing.zapfileuploadmw.thrift.TFileHeader;
import com.vng.zing.zapfileuploadmw.thrift.TFileHeaderEx;
import com.vng.zing.zapfileuploadmw.thrift.TFileInfo;
import com.vng.zing.zapfileuploadmw.thrift.TFileResult;
import com.vng.zing.zapfileuploadmw.thrift.TMeta;
import com.vng.zing.zcommon.thrift.ECode;
import com.vng.zing.zcommon.thrift.PutPolicy;
import com.vng.zing.zeventbus.Notifier;
import com.vng.zing.zmcloud.thrift.ZMCProcessResult;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;

/**
 * @author nhantt4
 */

public class TFileUploadModel {

	private static final Class ThisClass = TFileUploadModel.class;
	private static final Logger _log = ZLogger.getLogger(ThisClass);
	public static final TFileUploadModel Instance = new TFileUploadModel();
	public static final IdZenClient _idZenFile;
	public static final Notifier _notifierStorage;
	public static String _eventUploadChunkName;
	public static String _keyIdZenFile;
	
	
	public static final long _secretCodeVideo;
	public static final long _hotThreshold;
	public static final ServerFarm _serverFarm;
	
	
	public static ServerFarm _serverHlsFarm = null;
	public static ServerFarm _serverFarmOA = null;
	public static ServerFarm _serverHlsFarmOA = null;
	
	
	public static EUploadType _modelUploadType;
	public static EQualityType _modelQuality;
	
	
	// use to avoice duplicate file
	public static ZiCache<String, TFileResult> _fileFinishCache;

	public static String _storagePath;
	private static final String DOMAIN_URL;
	
	
	private static final boolean _enableMCloud;
	public static ZiDBCoreClient<TFileResult> _headerClient;
	
	
	public static List64Client _chunkClient;
	
	
	private static final PutEntryOption peo;
	private static final Random _rand;
	private static final JZSMetaClient _metaClient;

	public static boolean _enableNotifyNewFile;
	public static final Notifier _notiferFileInfo;
	public static String _eventNewFile;

	public static final StrCounter64Client _hotCounterClient;
	public static final StrListStrClient _hotListClient;

	private static final boolean _isHlsEnable;
	private static final boolean _isThumbnailEnable;
	private static final boolean _isUseDomainForOA;
	private static final boolean _isSupportHTTPUpload;
	private static final boolean _enableCheckBitRate;

	private static final List<Integer> OA_WHITELIST = new ArrayList();

	//Video Chat HTTP
	public static IdZenClient _idZenFileHTTP = null;
	public static String _keyIdZenFileHTTP;
	public static Notifier _notifierChatHTTPStorage = null;
	public static String _eventUploadChatHTTPName;
	public static ServerFarm _serverHTTPFarm = null;

	private static final long MAX_BITRATE_ALLOWED = 2000;

	//--- slotlock
	private static final List<Object> _slotLock = new ArrayList<Object>();
	private static final int NSLOTLOCK = 128;

	static {
		_log.info("Start TFileUploadModel: _modelUploadType=" + _modelUploadType);

		_secretCodeVideo = ZConfig.Instance.getLong(ThisClass, "main", "secret_code_video", 0);
		_isHlsEnable = ZConfig.Instance.getBoolean(ThisClass, "main", "is_hls_enable", false);
		_hotThreshold = ZConfig.Instance.getLong(ThisClass, "main", "hot_threshold", 0);
		_isThumbnailEnable = ZConfig.Instance.getBoolean(ThisClass, "main", "enable_create_thumb", false);
		_isUseDomainForOA = ZConfig.Instance.getBoolean(ThisClass, "main", "use_domain_for_oa", false);
		_isSupportHTTPUpload = ZConfig.Instance.getBoolean(ThisClass, "main", "is_support_http_upload", false);
		_enableCheckBitRate = ZConfig.Instance.getBoolean(ThisClass, "main", "enable_check_bitrate", false);
		if (_enableCheckBitRate) {
			String[] stringArray = ZConfig.Instance.getStringArray(ThisClass, "main", "oa_whitelist", new String[0]);
			if (stringArray != null && stringArray.length > 0) {
				for (String string : stringArray) {
					OA_WHITELIST.add(NumberUtils.toInt(string, 0));
				}
			}
		}
		String uploadType = ZConfig.Instance.getString(ThisClass, "main", "upload_type", "");
		_enableNotifyNewFile = ZConfig.Instance.getBoolean(Notifier.class, "fileinfo", "enable", false);
		_serverFarm = new ServerFarm(uploadType);
		if (_isHlsEnable) {
			_serverHlsFarm = new ServerFarm(uploadType + "_hls");
		}
		if (_isUseDomainForOA) {
			_serverFarmOA = new ServerFarm(uploadType + "_oa");
			if (_isHlsEnable) {
				_serverHlsFarmOA = new ServerFarm(uploadType + "_hls_oa");
			}
		}
		String evtName = "evt_name_upload_chunk";
		if ("photo_chat".equals(uploadType)) {
			_modelUploadType = EUploadType.PHOTO_CHAT;
		} else if ("photo_doodle".equals(uploadType)) {
			_modelUploadType = EUploadType.PHOTO_DOODLE;
		} else if ("photo_draw".equals(uploadType)) {
			_modelUploadType = EUploadType.PHOTO_DRAW;
		} else if ("voice_chat".equals(uploadType)) {
			_modelUploadType = EUploadType.VOICE_CHAT;
		} else if ("voice_social".equals(uploadType)) {
			_modelUploadType = EUploadType.VOICE_SOCIAL;
		} else if ("video_chat".equals(uploadType)) {
			_modelUploadType = EUploadType.VIDEO_CHAT;
		} else if ("video_social".equals(uploadType)) {
			_modelUploadType = EUploadType.VIDEO_SOCIAL;
		} else if ("share_file".equals(uploadType)) {
			_modelUploadType = EUploadType.SHARE_FILE;
			evtName = "evt_name_share_file_chunk";
		} else {
			// wrong config upload type, STOP service
			_log.error("Wrong config upload_type");
			System.exit(1);
		}
		_log.info("Start TFileUploadModel: _modelUploadType=" + _modelUploadType);

		_idZenFile = new IdZenClient(uploadType);
		_keyIdZenFile = ZConfig.Instance.getString(IdZenClient.class, uploadType, "key", "");

		_notifierStorage = new Notifier("uploadchunk");
		_enableMCloud = ZConfig.Instance.getBoolean(ThisClass, uploadType, "enable_use_mcloud", false);
		if (_enableMCloud && evtName != "evt_name_share_file_chunk") {
			evtName = "evt_name_upload_chunk_mcloud";
		}
		_eventUploadChunkName = ZConfig.Instance.getString(Notifier.class, "uploadchunk", evtName, "");

		//Config upload via HTTP
		if (_isSupportHTTPUpload) {
			final String name = uploadType + "_http";
			_idZenFileHTTP = new IdZenClient(name);
			_keyIdZenFileHTTP = ZConfig.Instance.getString(IdZenClient.class, name, "key", "");
			_notifierChatHTTPStorage = new Notifier(name);
			_eventUploadChatHTTPName = ZConfig.Instance.getString(Notifier.class, name, "evt_name_upload_chat_http", "");
			_serverHTTPFarm = new ServerFarm(name);
		}

		_headerClient = new ZiDBCoreClient<TFileResult>(TFileResult.class, uploadType);
		_chunkClient = new List64Client(uploadType);
		_storagePath = ZConfig.Instance.getString(ThisClass, uploadType, "storage_path", "");
		DOMAIN_URL = ZConfig.Instance.getString(ThisClass, uploadType, "domain_url", "");

		peo = new PutEntryOption();
		peo.setAddUndup(true);
		peo.setAddWay(AddWay.PUSH_BACK);
		peo.setCheckDup(true);
		peo.setMedFind(FindMed.SEQ_BEG);
		peo.setDupBehav(DupBehav.REJECT);

		_fileFinishCache = new ZiCache<String, TFileResult>("filedata",
				new IZStructor.StringStructor(), new IZStructor() {
			@Override
			public TFileResult ctor() {
				return new TFileResult();
			}
		});

		_rand = new Random();
		_metaClient = new JZSMetaClient(uploadType);

		_enableNotifyNewFile = ZConfig.Instance.getBoolean(Notifier.class, "fileinfo", "enable", false);
		_notiferFileInfo = new Notifier("fileinfo");
		_eventNewFile = ZConfig.Instance.getString(Notifier.class, "fileinfo", "evt_name_newfile", "");
		///
		_hotCounterClient = new StrCounter64Client("zvideo");
		_hotListClient = new StrListStrClient("zvideo_by_size");


		//--- slotlock
		_slotLock.clear();
		for (int i = 0; i < NSLOTLOCK; i++) {
			_slotLock.add(new String("lockObj_" + (i + 1)));
		}
	}

	public static void init() {
		_idZenFile.get(_keyIdZenFile);
		Profiler.createThreadProfiler("init", false);
		Profiler.closeThreadProfiler();
	}

	public long currentFileId() {
		return _idZenFile.get(_keyIdZenFile).value;
	}

	public long nextFileId() {
		return _idZenFile.gnext(_keyIdZenFile).value;
	}

	public long nextFileIdViaHTTP() {
		return _idZenFileHTTP.gnext(_keyIdZenFileHTTP).value;
	}

	private Object _getSlotLock(int userId) {
		int slot = userId % _slotLock.size();
		return _slotLock.get(slot);
	}

	// rout user by profile
	private boolean _isOARout(int uid) {
		// default or on error: result is user
		com.vng.zalo.profile.thrift.TValue profile = ZaloProfileModelWithMCZiCache.INSTANCE.getProfileTValue(uid);
		return !(profile == null || profile.getType() <= 0);
	}

	//Check if OA user uploaded video using tool
	private boolean isOAFromTool(int uid, TFileHeaderEx exhdr) {
		//trilm2: specical OA account, disable check bit rate 
		if (OA_WHITELIST.contains(uid)) {
			_log.info("Skip check bit rate for OA id: " + uid);
			return false;
		}

		if (exhdr != null && exhdr.destId == 3 && exhdr.destType == 12) {
			boolean isOA = _isOARout(uid);
			_log.info(uid + " is a OA : " + isOA);
			return isOA;
		}
		_log.info("exhdr is null");
		return false;
	}

	private static final short ZERO = 0;

	private TFileResult _getMasterDetail(String strFileId, TFileHeader hdr, TFileResult result) throws Exception {
		String pathFile = "";
		String msgErr = "";
		ThreadProfiler profiler = Profiler.getThreadProfiler();
		//--- thông tin đường dẫn localpath
		profiler.push(ThisClass, "_headerClient.get");
		TFileResult masterChunk = new TFileResult();
		long retFirst = _headerClient.get(strFileId, masterChunk);
		profiler.pop(ThisClass, "_headerClient.get");
		if (retFirst < 0) {
			// first chunk
			pathFile = Util.createNowDirectory(_storagePath) + "/" + strFileId;
			Map<Short, String> map = new TreeMap<Short, String>();
			map.put(ZERO, pathFile);

			// write record
			masterChunk = new TFileResult(result);
			masterChunk.setFileHeader(hdr);
			masterChunk.setMapUrl(map);
			profiler.push(ThisClass, "_headerClient.put");
			long err = _headerClient.put(strFileId, masterChunk, PutPolicy.ADD_OR_UDP);
			profiler.pop(ThisClass, "_headerClient.put");
			if (err < 0) {
				msgErr = "_headerClient.put < 0: " + err;
				throw new Exception(msgErr);
			}
		} else {
			//--- kiểm tra tính hợp lệ của chunk (old)
			if ((masterChunk.mapUrl == null)
					|| (!masterChunk.mapUrl.containsKey(ZERO))) {
				result.setError(-ECode.NULL_PTR.getValue());
				msgErr = "firstResult.mapUrl == null";
				throw new Exception(msgErr);
			}

			if ((masterChunk.fileHeader == null || masterChunk.fileHeader.totalChunk == 0)
					&& (hdr != null && hdr.totalChunk > 0)) {
				masterChunk.setFileHeader(hdr);
				profiler.push(ThisClass, "_headerClient.put_masterChunk");
				long err = _headerClient.put(strFileId, masterChunk, PutPolicy.ADD_OR_UDP);
				profiler.pop(ThisClass, "_headerClient.put_masterChunk");
			}
		}
		return masterChunk;
	}

	private TFileResult _checkComplete(long fileId, String pathFile,
			TFileHeader hdrMaster, TFileHeaderEx exhdr) throws Exception {
		ThreadProfiler profiler = Profiler.getThreadProfiler();
		TFileResult result = new TFileResult();
		String strFileId = fileId + "";
		String msgErr = "";
		profiler.push(ThisClass, "_chunkClient.get");
		com.vng.zing.list64.thrift.TValueResult val = _chunkClient.get(fileId);
		profiler.pop(ThisClass, "_chunkClient.get");
		if (val.error < 0) {
			msgErr = "_chunkClient.get: " + val.error;
			throw new Exception(msgErr);
		}

		if (val.value.entries.size() == hdrMaster.totalChunk) {
			// kiểm tra lại để đảm bảo đúng thứ tự 1->n
			List<Long> lsChunkId = val.value.entries;
			boolean isEnough = true;
			short chunkBegin = 1;
			short chunkEnd = (short) (lsChunkId.size() - 1);

			for (short id = chunkBegin; id <= chunkEnd; id++) {
				if (!lsChunkId.contains((long) id)) {
					isEnough = false;
					msgErr = "chunkId.lack. id: " + id;
					throw new Exception(msgErr);
					//break;
				}
			}

			if (isEnough) {
				if (_enableCheckBitRate && isOAFromTool(hdrMaster.userId, exhdr)) {
					ThreadProfiler threadProfiler = Profiler.getThreadProfiler();
					threadProfiler.push(this.getClass(), "getBitRate");
					long bitRate = Util.getBitRate(pathFile);
					threadProfiler.pop(this.getClass(), "getBitRate");
					if (bitRate > MAX_BITRATE_ALLOWED) {
						_log.error(String.format("REACH_MAX\t%d\t%d\t%s", hdrMaster.userId, hdrMaster.cliFileId, pathFile));
						result.setError(ECode.REACH_MAX.getValue());
						return result;
					}
					_log.info(String.format("CHK_BITRATE\t%d\t%d\t%s\t%d", hdrMaster.userId, hdrMaster.cliFileId, pathFile, bitRate));
				}

				Map<Short, String> retMap = new TreeMap<Short, String>();
				retMap.put(ZERO, pathFile);
				result.setMapUrl(retMap);
				result.setError(EUploadError.UPLOAD_OK.getValue());

				long randNum = _rand.nextLong();
				long randSign = Math.abs(randNum);
				result.setCheckSum(randSign);
				if (_enableMCloud) {
					_processCompleteChunkMCloud(result, strFileId, hdrMaster, randSign);
				} else {
					_processCompleteChunk(result, strFileId, hdrMaster, randSign);
				}
				if (_enableNotifyNewFile) {
					TFileInfo fi = new TFileInfo();
					fi.setFileHeader(hdrMaster);
					fi.setFileResult(result);
					fi.setQualityType(_modelQuality);
					fi.setUploadType(_modelUploadType);
					if (exhdr != null) {
						fi.setHeaderEx(exhdr);
					}
					long notify = _notiferFileInfo.notify(_eventNewFile, fi);
				}
			} else {
				result.setError(-ECode.INVALID_PARAM.getValue());
			}
		} else {
			result.setError(EUploadError.UPLOAD_NOT_ENOUGH_CHUNK.getValue());
		}
		return result;
	}

	public TFileResult uploadChunk(TChunkData chunkData) {
		TFileResult result = new TFileResult();
		result.setError(-ECode.C_FAIL.getValue());
		ThreadProfiler profiler = Profiler.getThreadProfiler();
		String msgErr = "";

		int userId = chunkData.fileHeader.userId;
		Object slot = _getSlotLock(userId);
		synchronized (slot) {
			try {
				long fileId = Util.getFileId(
						chunkData.fileHeader.userId,
						chunkData.fileHeader.cliFileId);

				String strFileId = fileId + "";
				profiler.push(ThisClass, "_fileDataCache.get");
				TFileResult dataCache = _fileFinishCache.get(strFileId);
				profiler.pop(ThisClass, "_fileDataCache.get");
				if (dataCache != null) {
					//dataCache.setFileId(-1);
					dataCache.setError(-ECode.DUPLICATED.getValue());
					profiler.push(ThisClass, "duplicate");
					profiler.pop(ThisClass, "duplicate");
					_log.error("DUPLICATED: " + chunkData);
					return dataCache;
				}

				//--- general var
				short chunkOrder = (short) chunkData.fileHeader.chunkId;
				TFileResult masterChunk = _getMasterDetail(strFileId, chunkData.fileHeader, result);
				String pathFile = masterChunk.getMapUrl().get(ZERO);

				// put chunkId record
				profiler.push(ThisClass, "_chunkClient.syn_putEntry");
				long ret = _chunkClient.syn_putEntry(fileId, (long) chunkOrder, peo);
				profiler.pop(ThisClass, "_chunkClient.syn_putEntry");
				if (ret < 0 && ret != -ECode.DUPLICATED.getValue()) {
					msgErr = "_chunkClient.syn_putEntry: " + ret;
					throw new Exception(msgErr);
				}

				//---
				long errWrite = _writeChunkData(pathFile, chunkData);
				if (errWrite != ECode.C_SUCCESS.getValue()) {
					profiler.push(ThisClass, "renew");
					/// rebuild path file
					pathFile = Util.createNowDirectory(_storagePath) + "/" + strFileId;
					Map<Short, String> map = new TreeMap<Short, String>();
					map.put(ZERO, pathFile);
					TFileResult write = new TFileResult(result);
					write.setMapUrl(map);
					long err = _headerClient.put(strFileId, write, PutPolicy.ADD_OR_UDP);
					profiler.pop(ThisClass, "renew");
					_chunkClient.syn_remove(fileId);
					result.setError(-EUploadError.UPLOAD_FORCE_IS_NEW.getValue());
				} else {
					// check complete
					return _checkComplete(fileId, pathFile, masterChunk.fileHeader, chunkData.headerEx);
				}
			} catch (Exception ex) {
				_log.error(ex.getMessage() + "; chunkData=" + chunkData, ex);
				result.setError(-ECode.EXCEPTION.getValue());
				result.setMapUrl(new TreeMap<Short, String>());
				msgErr = ex.toString();
			}
		}

		if (result.error >= 0) {
			_log.info("uploadChunk: ret: " + result.error
					+ ", header=" + chunkData.fileHeader
					+ ", headerEx=" + chunkData.headerEx);
		} else {
			_log.warn("uploadChunk: ret: " + result.error
					+ ", msg: " + msgErr + ", header=" + chunkData.fileHeader
					+ ", headerEx=" + chunkData.headerEx);
		}

		return result;
	}

	private long _writeChunkData(String path, TChunkData chunkData) {
		long ret = ECode.C_SUCCESS.getValue();
		ThreadProfiler profiler = Profiler.getThreadProfiler();
		profiler.push(ThisClass, "writeChunkData");

		try {
			Profiler.getThreadProfiler().push(ThisClass, "_writeChunkData");

			int totalSize = chunkData.getFileHeader().totalSize;
			int pos;
			short totalChunk = chunkData.getFileHeader().totalChunk;
			short chunkIdOffset = (short) (chunkData.getFileHeader().chunkId - 1); // index begin on 0
			short lastId = (short) (totalChunk - 1);
			int sizePerChunk;

			File file = new File(path);
			if (!file.exists()) {
				Util.createFile(path, totalSize);
			}

			if (chunkIdOffset != lastId) {
				sizePerChunk = chunkData.getData().length;
				pos = chunkIdOffset * sizePerChunk;
			} else {
				// last chunk
				pos = totalSize - chunkData.getData().length;
			}

			Util.writeFile(path, chunkData.getData(), pos);
			_log.info("chunkId(" + chunkIdOffset + ") -> write: " + path);
		} catch (Exception ex) {
			_log.error(ex.getMessage(), ex);
			ret = -ECode.EXCEPTION.getValue();
		} finally {
			Profiler.getThreadProfiler().pop(ThisClass, "_writeChunkData");
		}

		profiler.pop(ThisClass, "writeChunkData");
		return ret;
	}

	// Build map URL for Flow Normal
	private Map<Short, String> _buildMapUrl(long fileId, long randCheck, TFileResult result, long photoId) {
		Map<Short, String> mapUrl = new TreeMap<Short, String>();
		// zen mp4 url
		String noiseId = ZCommonX.noise64(fileId, _secretCodeVideo);
		ServerFarm farm = _serverFarm; //_getFarmByUid(header.userId);
		noiseId = ZCommonX.noise64(fileId, _secretCodeVideo);
		String domain = farm.getDomain(result.nodeId);
		mapUrl.put((short) 0, String.format("%s%s/%d", domain, noiseId, randCheck));

		// zen hls url
		if (_isHlsEnable) {
			String hlsDomain = _serverHlsFarm.getDomain(result.hlsNodeId);
			String hlsUrl = Util.zenHlsUrl(hlsDomain, fileId);
			mapUrl.put((short) 1, hlsUrl);
		}

		if (_isThumbnailEnable) {
			//create thumbnail
			if (photoId > 0) {
				Map<Short, String> photoMapUrl = VideoThumbModel.INSTANCE.getMapUrl(photoId);
				if (photoMapUrl != null && !photoMapUrl.isEmpty()) {
					String thumbnailUrl = photoMapUrl.get((short) 1);
					mapUrl.put((short) 2, thumbnailUrl == null ? "" : thumbnailUrl);
				}
			} else {
				mapUrl.put((short) 2, "");
				_log.error("gen photoId FAIL - photoId=" + photoId + ",fileId=" + fileId);
			}
		}
		return mapUrl;
	}

	// Build map URL for Flow MCloud
	private void _buildMapUrl(long fileId, long photoId, Map<Short, String> mapUrl) {
		//create thumbnail
		if (photoId > 0) {
			Map<Short, String> photoMapUrl = VideoThumbModel.INSTANCE.getMapUrl(photoId);
			if (photoMapUrl != null && !photoMapUrl.isEmpty()) {
				String thumbnailUrl = photoMapUrl.get((short) 1);
				mapUrl.put((short) 2, thumbnailUrl == null ? "" : thumbnailUrl);
			}
		} else {
			mapUrl.put((short) 2, "");
			_log.error("gen photoId FAIL - photoId=" + photoId + ",fileId=" + fileId);
		}
	}
	
	// Build map URL for OA (Flow Normal)	
	private Map<Short, String> _buildMapUrlForOA(long fileId, long randCheck, TFileResult result, long photoId) {
		Map<Short, String> mapUrl = new TreeMap<Short, String>();
		// zen mp4 url
		String noiseId = ZCommonX.noise64(fileId, _secretCodeVideo);
		String domain = _serverFarmOA.getDomain(result.nodeId);
		mapUrl.put((short) 0, String.format("%s%s/%d", domain, noiseId, randCheck));

		// zen hls url
		if (_isHlsEnable) {
			String hlsDomain = _serverHlsFarmOA.getDomain(result.hlsNodeId);
			String hlsUrl = Util.zenHlsUrl(hlsDomain, fileId);
			mapUrl.put((short) 1, hlsUrl);
		}

		if (_isThumbnailEnable) {
			//create thumbnail
			if (photoId > 0) {
				Map<Short, String> photoMapUrl = VideoThumbModel.INSTANCE.getMapUrl(photoId);
				if (photoMapUrl != null && !photoMapUrl.isEmpty()) {
					String thumbnailUrl = photoMapUrl.get((short) 1);
					mapUrl.put((short) 2, thumbnailUrl == null ? "" : thumbnailUrl);
				}
			} else {
				mapUrl.put((short) 2, "");
				_log.error("gen photoId FAIL - photoId=" + photoId + ",fileId=" + fileId);
			}
		}
		return mapUrl;
	}

	private void _procHotFile(TFileResult file) {
		ThreadProfiler profiler = Profiler.getThreadProfiler();
		profiler.push(ThisClass, "_procHotFile");
		try {
			long fileSize = file.fileHeader.totalSize;
			if (!file.mapUrl.containsKey((short) 0)) {
				return;
			}

			String strUrl = file.mapUrl.get((short) 0);
			String strKeySize = fileSize + "";
			com.vng.zing.strliststr.thrift.PutEntryOption peo = new com.vng.zing.strliststr.thrift.PutEntryOption();
			long increase = _hotCounterClient.increaseWithCreateKey(strKeySize, 1);
			com.vng.zing.strcounter64.thrift.TValueResult get = _hotCounterClient.get(strKeySize);
			if (get.value < _hotThreshold) {
				return;
			}

			long putEntry = _hotListClient.putEntry(strKeySize, strUrl, peo);
			// put hot eAllMap
			String strHotKey = "__hot";
			_hotListClient.putEntry(strHotKey, strKeySize, peo);
		} finally {
			profiler.pop(ThisClass, "_procHotFile");
		}
	}

	// Flow Normal
	private boolean _processCompleteChunk(TFileResult _return, String strFileId,
			TFileHeader hdrMaster, long randChk) {
		ThreadProfiler profiler = Profiler.getThreadProfiler();
		int error = ECode.C_SUCCESS.getValue();
		long fileIdZen = nextFileId();
		if (fileIdZen <= 0) {
			_return.setError(-ECode.C_FAIL.getValue());
			_log.error("nextFileId() FAIL: " + fileIdZen);
			return false;
		}

		ServerFarm farm = _serverFarm;
		_return.setNodeId(farm.getNodeId(fileIdZen)); //set farm node id for video uploaded
		if (_isHlsEnable) {
			short hlsNodeId = _serverHlsFarm.getNodeId(fileIdZen);
			_return.setHlsNodeId(hlsNodeId); //set hls farm id for video uploaded
		}
		profiler.push(ThisClass, "_metaClient.get");
		com.vng.zing.zbfmeta.thrift.TValueResult chkExist = _metaClient.get(fileIdZen);
		profiler.pop(ThisClass, "_metaClient.get");
		if (chkExist.error != -ECode.NOT_EXIST.getValue()) {
			_log.error("nextFileId() DUPLICATED: " + fileIdZen + ", err_code: " + chkExist.error);
			error = -ECode.DUPLICATED.getValue();
			_return.setError(error);
			return false;
		}

		// request by NamLQ
		com.vng.zing.zbfmeta.thrift.TValue metaInfo = new com.vng.zing.zbfmeta.thrift.TValue();
		metaInfo.setChunkSize(0);
		profiler.push(ThisClass, "_metaClient.put");
		long retPutMeta = _metaClient.put(fileIdZen, metaInfo, PutPolicy.ADD);
		profiler.pop(ThisClass, "_metaClient.put");
		if (retPutMeta < 0) {
			_log.error("put meta FAIL: " + retPutMeta);
			error = -ECode.C_FAIL.getValue();
			_return.setError(error);
			return false;
		}

		profiler.push(ThisClass, "completeChunk");
		try {
			if (_modelUploadType == EUploadType.VIDEO_CHAT
					|| _modelUploadType == EUploadType.SHARE_FILE) {
				_return.setFileId(fileIdZen);
				_return.setFileHeader(hdrMaster);
				//_return.fileHeader.setCliFileId(fileIdZen);
				// _isOARout(hdrMaster.userId) && _isOAConvertHls

				long photoId = 0;
				if (_isThumbnailEnable) {
					photoId = VideoThumbModel.INSTANCE.getNextId();
					if (photoId <= 0) {
						_log.error("getNextIdPhoto FAIL: " + photoId + ",strFileId=" + strFileId + ",fileIdZen=" + fileIdZen);
						error = -ECode.C_FAIL.getValue();
						_return.setError(error);
						return false;
					}
					if (_return.fileHeader.meta == null) {
						TMeta storyMeta = new TMeta();
						storyMeta.setPhotoId(photoId);
						_return.fileHeader.setMeta(storyMeta);
					} else {
						_return.fileHeader.meta.setPhotoId(photoId);
					}
				}

				profiler.push(ThisClass, "notify");
				
				// Event có tên video_process => gọi bên convert
				long notify = _notifierStorage.notify(_eventUploadChunkName, _return);
				_log.info("eventName=" + _eventUploadChunkName + ", notifier= " + _return.toString() + ", error=" + notify);
				profiler.pop(ThisClass, "notify");

				if (_isUseDomainForOA && _isOARout(hdrMaster.userId)) {// Official Account
					Map<Short, String> mapUrl = _buildMapUrlForOA(fileIdZen, randChk, _return, photoId);
					_return.setMapUrl(mapUrl);
				} else {// user
					Map<Short, String> mapUrl = _buildMapUrl(fileIdZen, randChk, _return, photoId);
					_return.setMapUrl(mapUrl);
				}
				if (_modelUploadType == EUploadType.VIDEO_CHAT) {
					_procHotFile(_return);
				}

				_return.setError(EUploadError.UPLOAD_OK.getValue());
				profiler.push(ThisClass, "_fileFinishCache.put");
				_fileFinishCache.put(strFileId, _return, com.vng.zing.common.PutPolicy.ADD_OR_UDP);
				profiler.pop(ThisClass, "_fileFinishCache.put");
				_log.info("completeChunk: " + _return);
			} else {
				error = -ECode.C_FAIL.getValue();
			}
		} catch (Exception ex) {
			_log.error(ex.getMessage(), ex);
		} finally {
			Profiler.getThreadProfiler().pop(ThisClass, "completeChunk");
		}
		_return.setError(error);
		return error == ECode.C_SUCCESS.getValue();
	}

	// Flow MCloud
	private boolean _processCompleteChunkMCloud(TFileResult _return, String strFileId,
			TFileHeader hdrMaster, long randChk) {
		int error = ECode.C_SUCCESS.getValue();
		long fileIdZen = nextFileId();
		if (fileIdZen <= 0) {
			_return.setError(-ECode.C_FAIL.getValue());
			_log.error("nextFileId() FAIL: " + fileIdZen);
			return false;
		}

		boolean autoGenThumb = !_isThumbnailEnable;
		String pathFile = _return.mapUrl.get(ZERO);
		String urlFile = DOMAIN_URL + StringUtils.substring(pathFile, _storagePath.length());

		boolean isOA = _isOARout(hdrMaster.userId);

		ZMCProcessResult processRet = ZMediaServiceModel.INSTANCE.process(urlFile, autoGenThumb, isOA);
		if (ZErrorHelper.isFail(processRet.error)) {
			error = -ECode.C_FAIL.getValue();
			_log.info("completeChunk.proccess FAIL - err=" + processRet.error + "," + _return);
			_return.setError(error);
			return false;
		}

		ThreadProfiler profiler = Profiler.getThreadProfiler();
		profiler.push(ThisClass, "completeChunk");
		try {
			if (_modelUploadType == EUploadType.VIDEO_CHAT
					|| _modelUploadType == EUploadType.SHARE_FILE) {
				_return.setFileId(fileIdZen);
				_return.setFileHeader(hdrMaster);

				Map<Short, String> mapUrl = new TreeMap<Short, String>();
				long photoId = 0;
				if (_isThumbnailEnable) {
					photoId = VideoThumbModel.INSTANCE.getNextId();
					if (photoId <= 0) {
						_log.error("getNextIdPhoto FAIL: " + photoId + ",strFileId=" + strFileId + ",fileIdZen=" + fileIdZen);
						error = -ECode.C_FAIL.getValue();
						_return.setError(error);
						return false;
					}
					if (_return.fileHeader.meta == null) {
						TMeta storyMeta = new TMeta();
						storyMeta.setPhotoId(photoId);
						_return.fileHeader.setMeta(storyMeta);
					} else {
						_return.fileHeader.meta.setPhotoId(photoId);
					}

					profiler.push(ThisClass, "notify");
					
					// Event tạo thumb, vì đã gọi process video ở trên rồi (mcCloud)
					long notify = _notifierStorage.notify(_eventUploadChunkName, _return);
					_log.info("eventName=" + _eventUploadChunkName + ", notifier= " + _return.toString() + ", error=" + notify);
					profiler.pop(ThisClass, "notify");

					_buildMapUrl(fileIdZen, photoId, mapUrl);
				}
				mapUrl.put((short) 3, processRet.mediaId);
				_return.setMapUrl(mapUrl);
				if (_modelUploadType == EUploadType.VIDEO_CHAT) {
					_procHotFile(_return);
				}

				_return.setError(EUploadError.UPLOAD_OK.getValue());
				_fileFinishCache.put(strFileId, _return, com.vng.zing.common.PutPolicy.ADD_OR_UDP);
				_log.info("completeChunk: " + _return);
			} else {
				error = -ECode.C_FAIL.getValue();
			}
		} catch (Exception ex) {
			_log.error(ex.getMessage(), ex);
		} finally {
			Profiler.getThreadProfiler().pop(ThisClass, "completeChunk");
		}
		_return.setError(error);
		return error == ECode.C_SUCCESS.getValue();
	}

	public static void main(String[] args) {
//		long put = TFileUploadModel._idZenFile.put("snap_video", 2000000l, PutPolicy.ADD_OR_UDP);
//		String strMp4 = "/data/snap_video/20170117/14/478226006646";
//		String strHls = TFileUploadModel._procHls(strMp4, "123");
//		System.out.println("strHls=" + strHls);
//		System.out.println("DONE");
		System.out.println("value=" + Instance.isOAFromTool(456789123, null));

		System.out.println("bitRate = " + Util.getBitRate("/home/tamnd2/Downloads/data/350950598.mp4"));
		System.out.println("bitRate = " + Util.getBitRate("/home/tamnd2/Downloads/data/3384222.mp4"));
		System.out.println("bitRate = " + Util.getBitRate("/home/tamnd2/Downloads/data/hst_1.mpg"));
	}
}
