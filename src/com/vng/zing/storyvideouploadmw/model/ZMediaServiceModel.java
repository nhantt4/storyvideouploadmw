package com.vng.zing.storyvideouploadmw.model;

import com.vng.zing.logger.ZLogger;
import com.vng.zing.storyvideouploadmw.thrift.wrapper.ZMediaServiceClient;
import com.vng.zing.zmcloud.thrift.ZMCImageQuality;
import com.vng.zing.zmcloud.thrift.ZMCMediaInfoResult;
import com.vng.zing.zmcloud.thrift.ZMCOutputInfo;
import com.vng.zing.zmcloud.thrift.ZMCPosterSource;
import com.vng.zing.zmcloud.thrift.ZMCProcessOption;
import com.vng.zing.zmcloud.thrift.ZMCProcessResult;
import com.vng.zing.zmcloud.thrift.ZMCSourceInfo;
import com.vng.zing.zmcloud.thrift.ZMCSourceType;
import com.vng.zing.zmcloud.thrift.ZMCVideoFormat;
import com.vng.zing.zmcloud.thrift.ZMCVideoQuality;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;

/**
 *
 * @author tamnd2
 */
public class ZMediaServiceModel {

	private static final Class THIS_CLASS = ZMediaServiceModel.class;
	private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
	public static final ZMediaServiceModel INSTANCE = new ZMediaServiceModel();

	private static final ZMediaServiceClient MEDIA_CLIENT = new ZMediaServiceClient("videostory");

	public static void main(String[] args) {
		ZMCMediaInfoResult mediaInfo = INSTANCE.getMediaInfo("125776l");
		System.out.println("mediaInfo = " + mediaInfo);

		String urlFile = "http://10.30.58.49:81/20170718/14/506601787884420465";
		ZMCProcessResult processRet = ZMediaServiceModel.INSTANCE.process(urlFile, true, false);
		System.out.println("processRet = " + processRet);
		System.exit(0);
	}

	public ZMCMediaInfoResult getMediaInfo(String mediaId) {
		return MEDIA_CLIENT.getMediaInfo(mediaId);
	}

	public ZMCProcessResult process(String inputUrl, boolean isGenThumb, boolean isOA) {
		ZMCProcessOption opt = new ZMCProcessOption();

		ZMCSourceInfo input = new ZMCSourceInfo();
		input.setSourceType(ZMCSourceType.HTTP.getValue());
		input.setPath(inputUrl);
		input.setIsExternalSource(false);
		opt.setMediaSource(input);

		Set<Integer> outFormats = buildOutputFormat();
		Set<Integer> outQuality = buildOutputVideoQuality();
		Map<Integer, Set<Integer>> formats = buildOutputFormat(outFormats, outQuality);

		ZMCOutputInfo output = new ZMCOutputInfo();
		output.setFormats(formats);

		opt.setGenPoster(isGenThumb);
		if (isGenThumb) {
			ZMCPosterSource posterSrc = new ZMCPosterSource();
			posterSrc.setAutoGen(true);
			posterSrc.setQuality(buildOutputImageQuality());
			opt.setPosterSource(posterSrc);
		}

		opt.setOutput(output);
		opt.setMediaName("");

		ZMCProcessResult processResult = null;
		if (isOA) {
			opt.setAppId(MEDIA_CLIENT.OA_APP_ID);
			processResult = MEDIA_CLIENT.processForOA(opt);
		} else {
			opt.setAppId(MEDIA_CLIENT.APP_ID);
			processResult = MEDIA_CLIENT.process(opt);
		}
		LOGGER.info("processResult: " + processResult);
		return processResult;
	}

	private static Map<Integer, Set<Integer>> buildOutputFormat(Set<Integer> outFormats, Set<Integer> outQualities) {
		Map<Integer, Set<Integer>> formats = new HashMap();
		for (Integer formatId : outFormats) {
			formats.put(formatId, outQualities);
		}
		return formats;
	}

	private static Set<Integer> buildOutputFormat() {
		Set<Integer> outFormats = new HashSet();
//		outFormats.add(ZMCVideoFormat.HLS.getValue());
//		outFormats.add(ZMCVideoFormat.HLS_V2.getValue());
		outFormats.add(ZMCVideoFormat.MP4_H264.getValue());
		return outFormats;
	}

	private static Set<Integer> buildOutputVideoQuality() {
		Set<Integer> outQuality = new HashSet();
//		outQuality.add(ZMCVideoQuality.QUALITY_720P.getValue());
		outQuality.add(ZMCVideoQuality.QUALITY_480P.getValue());
		return outQuality;
	}

	private static Set<Integer> buildOutputImageQuality() {
		Set<Integer> outQuality = new HashSet();
		outQuality.add(ZMCImageQuality.QUALITY_720P.getValue());
		return outQuality;
	}
}
