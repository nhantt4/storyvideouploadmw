package com.vng.zing.storyvideouploadmw.model;

import com.vng.zing.common.TBaseBuilder;
import com.vng.zing.common.ZErrorHelper;
import com.vng.zing.configer.ZConfig;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.stats.Profiler;
import com.vng.zing.stats.ThreadProfiler;
import com.vng.zing.strlist64.thrift.wrapper.StrList64Client;
import com.vng.zing.storyvideouploadmw.thrift.wrapper.ZAPPhotoModelWriteClient;
import com.vng.zing.zapfileuploadmw.thrift.TFileResult;
import com.vng.zing.zapphotomw.thrift.TPutPhotoData;
import com.vng.zing.zapphotomw.thrift.TPutPhotoResult;
import com.vng.zing.zeventbus.Watcher;
import com.vng.zing.zeventbus.ZEvbusSubsys;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author tamnd2
 */
public class VideoThumbModel {

	private static final Class ThisClass = VideoThumbModel.class;
	public static final VideoThumbModel INSTANCE = new VideoThumbModel();
	private static final Logger LOGGER = ZLogger.getLogger(VideoThumbModel.class);

	private static ZEvbusSubsys _ebVideoResize;
	private static final StrList64Client _failListClient;
	private static final ZAPPhotoModelWriteClient _photoClient;

	private static final String GET_THUMBNAIL_STRING_1 = "ffmpeg -y -i $INPUT_FILE -ss 00:00:01.000 -vframes 1 -vf scale=$SCALE $OUTPUT_FILE";
	private static final String GET_THUMBNAIL_STRING_0 = "ffmpeg -y -i $INPUT_FILE -ss 00:00:00.000 -vframes 1 -vf scale=$SCALE $OUTPUT_FILE";

	static {
		_failListClient = new StrList64Client("video_qos");
		_photoClient = new ZAPPhotoModelWriteClient("za_photo_story");
	}

	public void init() {
		Profiler.createThreadProfiler("init", false);
		Profiler.closeThreadProfiler();
	}

	public boolean registerVideoThumbnail() {
		//doc chung config de do mat cong doi config nhieu cho khi chuyen 
		// giua su sung mcloud va khong su dung mcloud
		String instName;
		String uploadType = ZConfig.Instance.getString(TFileUploadModel.class, "main", "upload_type", "");
		boolean _enableMCloud = ZConfig.Instance.getBoolean(TFileUploadModel.class, uploadType, "enable_use_mcloud", false);
		if (_enableMCloud) {
			instName = "video_thumb_v2";
		} else {
			instName = "video_thumb";
		}

		if (ZConfig.Instance.getBoolean(Watcher.class, instName, "enable", false)) {
			String eventName = ZConfig.Instance.getString(Watcher.class, instName, "eventsRecv_name", "zstory.video_process");

			LOGGER.info("_enableMCloud: " + _enableMCloud + ", eventBusName: " + instName + ", eventName: " + eventName);
			_ebVideoResize = new ZEvbusSubsys(instName);
			_ebVideoResize.setHandler(eventName, new Watcher.TBaseHandler() {
				@Override
				public void onMessageWatResDatasCome(long eventId, String eventName, int typeId, TBaseBuilder tbuilder) {
					Profiler.createThreadProfiler("onMessageWatResDatasCome", false);
					try {
						TFileResult videoData = new TFileResult();
						if (tbuilder.build(videoData)) {
							LOGGER.info("onMessageWatResDatasCome: " + videoData);
							process(videoData);
						}
					} catch (Exception ex) {
						LOGGER.error(ex.getMessage(), ex);
					} finally {
						Profiler.closeThreadProfiler();
					}
				}
			});
			_ebVideoResize.start();
			LOGGER.info("registerWatcher: " + eventName + " OK");
		} else {
			LOGGER.info("disable VideoThumb");
		}
		return false;
	}

	public long getNextId() {
		return _photoClient.nextPhotoId();
	}

	public Map<Short, String> getMapUrl(long photoId) {
		ThreadProfiler profiler = Profiler.getThreadProfiler();
		try {
			profiler.push(ThisClass, "getMapUrl");
			TPutPhotoData photo = new TPutPhotoData();
			com.vng.zing.zapphotometa.thrift.TValue meta = new com.vng.zing.zapphotometa.thrift.TValue();
			meta.setAlbumId(0);
			meta.setOwnerId(3);
			meta.setCreateDate(System.currentTimeMillis());
			meta.setDesc("");
			com.vng.zing.zapphotometa.thrift.TLocation location = new com.vng.zing.zapphotometa.thrift.TLocation();
			location.setId(1);
			location.setInfo("");
			meta.setLocation(location);
			meta.setFeedId(0); // feedId temporary = 0
			photo.setPhotoMeta(meta);
			photo.setPhotoId(photoId);

			TPutPhotoResult putPhoto = _photoClient.putPhoto(photo);
			LOGGER.info(putPhoto);
			if (ZErrorHelper.isSuccess(putPhoto.error)) {
				return putPhoto.mapUrl;
			}
			return null;
		} finally {
			profiler.pop(ThisClass, "getMapUrl");
		}
	}

	private void process(TFileResult videoData) {
		long videoId = videoData.fileId;
		if (videoData.isSetFileHeader()) {
			long photoId = 0;
			if (videoData.getFileHeader().isSetMeta()) {
				photoId = videoData.getFileHeader().meta.photoId;
			}

			if (photoId > 0) {
				String localVideoPath = videoData.mapUrl.get((short) 0);
				int dimensionHeight = 480;
				if (videoData.fileHeader.dimensionHeight > 0) {
					dimensionHeight = videoData.fileHeader.dimensionHeight >= 720 ? 720 : videoData.fileHeader.dimensionHeight;
				}
				int dimensionWidth = 480;
				if (videoData.fileHeader.dimensionWidth > 0) {
					dimensionWidth = videoData.fileHeader.dimensionWidth >= 720 ? 720 : videoData.fileHeader.dimensionWidth;
				}
				String scale;
				if (dimensionHeight >= dimensionWidth) {
					scale = "-1:" + dimensionHeight;
				} else {
					scale = dimensionWidth + ":-1";
				}

				String thumbnailLocalPath = createThumbnailVideo(localVideoPath, scale);
				if (thumbnailLocalPath != null) {
					try {
						byte[] data = FileUtils.readFileToByteArray(new File(thumbnailLocalPath));
						if (data.length > 0) {
							TPutPhotoResult uploadThumb = uploadThumb(photoId, data);
							LOGGER.info(uploadThumb);
						}
					} catch (IOException ex) {
						LOGGER.error(ex);
					}
				}
			} else {
				ThreadProfiler threadProfiler = Profiler.getThreadProfiler();
				threadProfiler.push(ThisClass, "noPhotoId");
				threadProfiler.pop(ThisClass, "noPhotoId");
			}
		}
	}

	private String runProcessCreateThumbnail(String processString, String filepath, String scale) {
		File inputFile = new File(filepath);
		if (!inputFile.exists()) {
			return null;
		}
		try {
			String outputPath = filepath + "_thumbnail.jpg";
			String execString = processString.replace("$INPUT_FILE", filepath)
					.replace("$OUTPUT_FILE", outputPath).replace("$SCALE", scale);
			LOGGER.info("Run command: " + execString);
			Process p = Runtime.getRuntime().exec(execString);

			//showProcessStream(p);
			boolean exitVal = p.waitFor(60, TimeUnit.SECONDS);

			File outputFile = new File(outputPath);
			if (!outputFile.exists()) {
				return null;
			}
			if (outputFile.length() > 0 || exitVal) {
				return outputFile.getAbsolutePath();
			} else {
				return null;
			}
		} catch (IOException ex) {
			LOGGER.error(ex);
			return null;
		} catch (InterruptedException ex) {
			LOGGER.error(ex);
			return null;
		}
	}

	private String createThumbnailVideo(String filepath, String scale) {
		ThreadProfiler prof = Profiler.getThreadProfiler();
		prof.push(ThisClass, "get_thumbnail");
		try {
			String outputPath = runProcessCreateThumbnail(GET_THUMBNAIL_STRING_1, filepath, scale);
			if (outputPath == null) {
				outputPath = runProcessCreateThumbnail(GET_THUMBNAIL_STRING_0, filepath, scale);
			}
			return outputPath;
		} catch (Exception e) {
			LOGGER.error(e.toString());
			return null;
		} finally {
			prof.pop(ThisClass, "get_thumbnail");
		}
	}

	public TPutPhotoResult uploadThumb(long photoId, byte[] arData) {
		ThreadProfiler profiler = Profiler.getThreadProfiler();
		profiler.push(ThisClass, "uploadThumb");
		int userId = 3;
		TPutPhotoResult ret = new TPutPhotoResult();
		try {
			TPutPhotoData photo = new TPutPhotoData();
			com.vng.zing.zapphotometa.thrift.TValue meta = new com.vng.zing.zapphotometa.thrift.TValue();
			meta.setAlbumId(0);
			meta.setOwnerId(userId);
			meta.setCreateDate(System.currentTimeMillis());
			meta.setDesc("");
			com.vng.zing.zapphotometa.thrift.TLocation location = new com.vng.zing.zapphotometa.thrift.TLocation();
			location.setId(1);
			location.setInfo("");
			meta.setLocation(location);
			meta.setFeedId(0); // feedId temporary = 0
			photo.setPhotoMeta(meta);

			com.vng.zing.photo.thrift.TValue rawData = new com.vng.zing.photo.thrift.TValue();
			rawData.setTimeLastUpdated(System.currentTimeMillis());
			rawData.setImage(arData);
			photo.setRawData(rawData);
			photo.setPhotoId(photoId);

			ret = _photoClient.putPhoto(photo);
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		} finally {
			profiler.pop(ThisClass, "uploadThumb");
		}

		if (ret.error >= 0) {
			return ret;
		} else {
			profiler.push(ThisClass, "PutPhotoThumbnailFail");
			LOGGER.info("Put photo thumbnail fail: " + ret);
			profiler.pop(ThisClass, "PutPhotoThumbnailFail");
		}
		return ret;
	}

	public static void main(String[] args) throws IOException {
//		Path fileLocation = Paths.get("/home/khoai/Desktop/box/thumb/thumbnail.jpg");
//		byte[] data = Files.readAllBytes(fileLocation);
//		TPutPhotoResult uploadThumb = VideoThumbModel.INSTANCE.uploadThumb(10054164688l, data);
//		System.out.println("uploadThumb=" + uploadThumb);

		String uploadType = ZConfig.Instance.getString(TFileUploadModel.class, "main", "upload_type", "");
		boolean _enableMCloud = ZConfig.Instance.getBoolean(TFileUploadModel.class, uploadType, "enable_use_mcloud", false);
		String eventConfig = "eventsRecv";
		if (_enableMCloud) {
			eventConfig = "eventsRecv_mcloud";
		}
		String eventName = ZConfig.Instance.getString(Watcher.class, "video_thumb", eventConfig, "");
		System.out.println(eventName);

	}
}
