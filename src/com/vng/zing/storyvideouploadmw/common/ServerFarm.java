package com.vng.zing.storyvideouploadmw.common;

import com.vng.zing.configer.ZConfig;
import com.vng.zing.logger.ZLogger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author tunt
 */
public class ServerFarm {

	private static final Logger _Logger = ZLogger.getLogger(Util.class);
	public HashMap<Short, String> _domains;

	private final String _name;
	private final List<Short> _nodeIds;

	public ServerFarm(String name) {
		_name = name;
		_domains = _getScaleUrlDomain();
		_nodeIds = new LinkedList<Short>();
		String cfgNodeIds = ZConfig.Instance.getString(ServerFarm.class, _name, "node_ids", "");
		List<String> listStrNodeId = new LinkedList<String>();
		listStrNodeId = Arrays.asList(cfgNodeIds.split(","));
		for (String strNode : listStrNodeId) {
			String[] split = strNode.split(":");
			if (split.length >= 2) {
				short nodeId = Short.parseShort(split[0]);
				short weight = Short.parseShort(split[1]);
				for (short idx = 0; idx < weight; idx++) {
					_nodeIds.add(nodeId);
				}
			} else {
				_nodeIds.add(Short.parseShort(split[0]));
			}
		}
	}

	public HashMap<Short, String> getDomains() {
		return _domains;
	}

	private HashMap<Short, String> _getScaleUrlDomain() {
		HashMap<Short, String> map = new HashMap<Short, String>();
		ArrayList<String> types = _getArrayConfig("node");
		for (String type : types) {
			String tmp[] = type.split("\\|");
			if (tmp.length >= 2) {
				try {
					map.put(Short.parseShort(tmp[0]), tmp[1]);
				} catch (Exception ex) {
					_Logger.error(ex.getMessage(), ex);
				}
			}
		}
		return map;
	}

	private ArrayList<String> _getArrayConfig(String key) {
		ArrayList<String> ret = new ArrayList<String>();
		for (int i = 1; i < 1024; ++i) {
			String str = ZConfig.Instance.getString(ServerFarm.class, _name,
					String.format("%s_%d", key, i), "");
			if (str == null || str.isEmpty()) {
				break;
			}
			ret.add(str);
		}
		return ret;
	}

	public short getNodeId(long pid) {
		return _nodeIds.get((int) (pid % _nodeIds.size()));
	}

	public String getDomain(short nodeId) {
		return _domains.get(nodeId);
	}

	public static void main(String[] args) {
		ServerFarm sf = new ServerFarm("photo");
	}
}
