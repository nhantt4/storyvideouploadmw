/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.vng.zing.storyvideouploadmw.common;

import com.vng.zing.logger.ZLogger;
import com.vng.zing.zcommon.thrift.ECode;
import com.vng.zing.zcommon.thrift.PutPolicy;
import com.vng.zing.zidb.thrift.TMapDataResult;
import com.vng.zing.zidb.thrift.TValue;
import com.vng.zing.zidb.thrift.TValueResult;
import com.vng.zing.zidb.thrift.wrapper.ZiDBClient;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.apache.thrift.TBase;
import org.apache.thrift.TDeserializer;
import org.apache.thrift.TException;
import org.apache.thrift.TSerializer;

/**
 *
 * @author dungln
 * @param <T> cài đặt cụ thể của kiểu TBase
 */
public class ZiDBCoreClient<T extends TBase> {

    private final Logger _Logger = ZLogger.getLogger(ZiDBCoreClient.class);
    private final ZiDBClient _ziClient;
    private final String _name;
    private final Class<T> _tClass;

    public ZiDBCoreClient(Class<T> tClass, String name) {
        _tClass = tClass;
        _name = name;
        _ziClient = new ZiDBClient(_name);
    }

    private void logInfo(String op, String id, String log) {
//        _Logger.info(String.format("ZiDBCoreClient(%s).%s key(%s): %s",
//                _name, op, id, log));
    }

    private void logError(String op, String id, Exception ex) {
        _Logger.error(String.format("ZiDBCoreClient(%s).%s key(%s)",
                _name, op, id), ex);
    }

    public long put(String id, T value, PutPolicy policy) {
        try {
            // key
            String strKey = id + "";
            ByteBuffer bId = ByteBuffer.wrap(strKey.getBytes(Charset.forName("UTF-8")));
            TSerializer serializer = new TSerializer();
            // value
            byte[] data = serializer.serialize(value);
            TValue tvalue = new TValue();
            tvalue.setData(ByteBuffer.wrap(data));

            long err = _ziClient.put(bId, tvalue, policy);
            if (err < 0) {
                logInfo("put", id, "err=" + err);
                return err;
            }
        } catch (TException ex) {
            logError("put", id, ex);
            return -ECode.C_FAIL.getValue();
        }
        return 0;
    }
	
    public long syn_put(String id, T value, PutPolicy policy) {
        try {
            // key
            String strKey = id + "";
            ByteBuffer bId = ByteBuffer.wrap(strKey.getBytes(Charset.forName("UTF-8")));
            TSerializer serializer = new TSerializer();
            // value
            byte[] data = serializer.serialize(value);
            TValue tvalue = new TValue();
            tvalue.setData(ByteBuffer.wrap(data));

            long err = _ziClient.syn_put(bId, tvalue, policy);
            if (err < 0) {
                logInfo("put", id, "err=" + err);
                return err;
            }
        } catch (TException ex) {
            logError("put", id, ex);
            return -ECode.C_FAIL.getValue();
        }
        return 0;
    }
	
    public boolean exist(String id) {
        // key
        String strKey = id + "";
        ByteBuffer bId = ByteBuffer.wrap(strKey.getBytes(Charset.forName("UTF-8")));
        TSerializer serializer = new TSerializer();
        // value
        int retExist = _ziClient.exist(bId);
        return retExist >= 0;
    }

    public long remove(String id) {
        // key
        String strKey = id + "";
        ByteBuffer bId = ByteBuffer.wrap(strKey.getBytes(Charset.forName("UTF-8")));
        long err = _ziClient.remove(bId);
        if (err < 0) {
            logInfo("remove", id, "err=" + err);
            return err;
        }
        return 0;
    }

    public long get(String id, T result) {
        long err = ECode.C_SUCCESS.getValue();
        try {
            // key
            String strKey = id + "";
            ByteBuffer bId = ByteBuffer.wrap(strKey.getBytes(Charset.forName("UTF-8")));
            TValueResult retGet = _ziClient.get(bId);
            if (retGet.error < 0 || retGet.value == null) {
                logInfo("get", id, "error=" + retGet.error);
                if (retGet.error < 0) {
                    err = retGet.error;
                } else {
                    err = -ECode.INVALID_DATA.getValue();
                }
                return err;
            }
            TDeserializer deserializer = new TDeserializer();
            deserializer.deserialize(result, retGet.value.data);
        } catch (TException ex) {
            logError("get", id, ex);
            err = -ECode.EXCEPTION.getValue();
        }
        return err;
    }

    public int multiGet(List<String> ids, Map<String, T> mapData, Map<String, Integer> mapError) {
        int error;
        // key
        List<ByteBuffer> lsKey = new ArrayList<ByteBuffer>();
        for (String id : ids) {
            ByteBuffer bId = ByteBuffer.wrap(id.getBytes(Charset.forName("UTF-8")));
            lsKey.add(bId);
        }

        TMapDataResult ret = _ziClient.multiGet(lsKey);

        error = ret.error;
        //Map<Long, Integer> errorMap = new TreeMap<Long, Integer>();
        if (ret.errorMap != null) {
            for (Map.Entry<ByteBuffer, Integer> entry : ret.errorMap.entrySet()) {
                ByteBuffer bId = entry.getKey();
                byte[] bytes = new byte[bId.remaining()];
                bId.get(bytes);
                String strId = new String(bytes);
                mapError.put(strId, entry.getValue());
            }
        }

        // dataMap
        if (ret.dataMap != null) {
            for (Map.Entry<ByteBuffer, TValue> entry : ret.dataMap.entrySet()) {
                ByteBuffer bId = entry.getKey();
                byte[] bytes = new byte[bId.remaining()];
                bId.get(bytes);
                String strId = new String(bytes, Charset.forName("UTF-8"));
                String id = strId;
                TValue tvalue = entry.getValue();
                try {
                    T value = _tClass.newInstance();
                    TDeserializer deserializer = new TDeserializer();
                    deserializer.deserialize(value, tvalue.data);
                    mapData.put(id, value);
                } catch (TException ex) {
                    logError("multiGet", id, ex);
                    error = -ECode.EXCEPTION.getValue();
                } catch (Exception ex) {
                    error = -ECode.EXCEPTION.getValue();
                }
            }
        }

        return error;
    }
//
//    public List<T> multiGet(List<Long> ids) {
//        List<T> res = new ArrayList<T>();
//        Map<Long, T> mapData = new TreeMap<Long, T>();
//        Map<Long, Integer> mapError = new TreeMap<Long, Integer>();
//        int ret_multi_get = multiGet(ids, mapData, mapError);
//        for (long id : ids) {
//            if (mapData.containsKey(id)) {
//                res.add(mapData.get(id));
//            }
//        }
//        return res;
//    }
//
    public long multiRemove(List<String> listId) {
        // key
        List<ByteBuffer> lsKey = new ArrayList<ByteBuffer>();
        for (String id : listId) {
            ByteBuffer bId = ByteBuffer.wrap(id.getBytes(Charset.forName("UTF-8")));
            lsKey.add(bId);
        }
        return _ziClient.multiRemove(lsKey);
    }

}

