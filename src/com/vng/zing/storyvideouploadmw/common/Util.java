package com.vng.zing.storyvideouploadmw.common;

import com.vng.zing.jni.zcommonx.wrapper.ZCommonX;
import com.vng.zing.logger.ZLogger;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.XMPDM;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

/**
 *
 * @author tunt
 */
public class Util {

	private static final org.apache.log4j.Logger _Logger = ZLogger.getLogger(Util.class);

	public static long getFileId(int userId, int clientFileId) {
		return (long) userId << 32 | clientFileId & 0xFFFFFFFFL;
	}

	public static String getChunkId(int userId, int clientFileId, short chunkId) {
		return String.format("%d_%d_%03d", userId, clientFileId, chunkId);
	}

	public static short fromStrChunkId(String strChunkId) {
		String[] tmp = strChunkId.split("_");
		short ret = -1;
		if (tmp.length > 0) {
			try {
				ret = Short.parseShort(tmp[tmp.length - 1]);
			} catch (Exception ex) {
			}
		}
		return ret;
	}

	public static String createNowDirectory(String rootDir) throws IOException {
		Date time = new Date();
		String rootDirStardard = rootDir;
		if (rootDir.charAt(rootDir.length() - 1) == '/') {
			rootDirStardard = rootDir.substring(0, rootDir.length() - 2);
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		String path = rootDirStardard + "/" + dateFormat.format(time)
				+ "/" + time.getHours();
		File dir = new File(path);
		if (!dir.exists()) {
			// create
			FileUtils.forceMkdir(dir);
		}
		return path;
	}

	public static long createFile(String path, int size) throws FileNotFoundException, IOException {
		RandomAccessFile raf = new RandomAccessFile(new File(path), "rw");
		try {
			raf.setLength((long) size);
		} finally {
			raf.close();
		}
		return size;
	}

	public static long writeFile(String path, byte[] data, int offset) throws FileNotFoundException, IOException {
		RandomAccessFile raf = new RandomAccessFile(new File(path), "rw");
		try {
			raf.seek(offset);
			raf.write(data);
		} finally {
			raf.close();
		}
		return offset;
	}

	public static long getFileSize(String path) {
		File file = new File(path);
		return file.length();
	}

	public static byte[] longToBytes(long x) {
		ByteBuffer buffer = ByteBuffer.allocate(Long.SIZE / 8);
		buffer.putLong(x);
		return buffer.array();
	}

	//public static String STORY_ZDN_APPNAME = "zs-mplaylist";
	public static long ID_NOISE_KEY = 1361504749304l;

	public static String zenHlsUrl(String nodeDomain, long id) {
		return String.format("%sm3u8?type=0&id=%s&q=4", nodeDomain, noiseId(id, ID_NOISE_KEY));
	}

	public static String noiseId(long item, long key) {
		return ZCommonX.noise64(item, key);
	}

	public static float getDurationInSec(String strDurattion) {
		if (strDurattion == null) {
			return 1;
		}
		if (!strDurattion.contains(":")) {
			return NumberUtils.toFloat(strDurattion, 1);
		}
		int hour = 0;
		int minute = 0;
		float seconds = 0;
		String[] split = StringUtils.split(strDurattion, ":");
		if (split != null && split.length > 0) {
			switch (split.length) {
				case 3:
					hour = NumberUtils.toInt(split[0], 0);
					minute = NumberUtils.toInt(split[1], 0);
					seconds = NumberUtils.toFloat(split[2], 0);
					break;
				case 2:
					minute = NumberUtils.toInt(split[1], 0);
					seconds = NumberUtils.toFloat(split[2], 0);
					break;
				case 1:
					seconds = NumberUtils.toFloat(split[2], 0);
					break;
			}
		}
		return (hour * 60 * 60) + (minute * 60) + (int) seconds;
	}

	public static long getBitRate(String path) {
		FileInputStream stream = null;
		try {
			Parser parser = new AutoDetectParser(); // Should auto-detect!
			ContentHandler handler = new BodyContentHandler();
			Metadata metadata = new Metadata();
			File file = new File(path);
			stream = new FileInputStream(file);
			parser.parse(stream, handler, metadata, new ParseContext());
			String durationStr = metadata.get(XMPDM.DURATION);

			float durationSec = getDurationInSec(durationStr);
			long bytes = file.length();
			long kbytes = (bytes / 1024);
			long bitRate = (long) ((kbytes * 8) / durationSec);
			return bitRate;
		} catch (FileNotFoundException ex) {
			_Logger.error(ex.getMessage(), ex);
		} catch (IOException ex) {
			_Logger.error(ex.getMessage(), ex);
		} catch (SAXException ex) {
			_Logger.error(ex.getMessage(), ex);
		} catch (TikaException ex) {
			_Logger.error(ex.getMessage(), ex);
		} catch (NumberFormatException ex) {
			_Logger.error(ex.getMessage(), ex);
		} finally {
			try {
				if (stream != null) {
					stream.close();
				}
			} catch (IOException ex) {
				_Logger.error(ex.getMessage(), ex);
			}
		}
		return 0;
	}
        
        public static long getContentLengthFromURL2(String fileURL) {
        int ret = -1;
        try {
            URL url = new URL(fileURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setInstanceFollowRedirects(false);
            //System.out.println("location:" + connection.getHeaderField("Location"));
            //System.out.println("code:" + connection.getResponseCode());
            ret = connection.getContentLength();
            if (ret <= 0) {
                _Logger.info("Invalid url:" + fileURL);
            }
        } catch (Exception ex) {
            _Logger.error(ex.getMessage(), ex);
        }
        return ret;
    }
}
