/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zing.storyvideouploadmw.handlers;

import com.vng.zing.zapfileuploadmw.thrift.ZAPFileUploadMWService;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.stats.Profiler;
import com.vng.zing.storyvideouploadmw.model.TFileUploadModel;
import com.vng.zing.zapfileuploadmw.thrift.TChunkData;
import com.vng.zing.zapfileuploadmw.thrift.TFileResult;
import com.vng.zte.zcommcommon.TZaloWorkerMessage;
import java.nio.ByteBuffer;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * @author namnq
 */

public class TFileUploadServiceHandler implements ZAPFileUploadMWService.Iface {

	private static final Logger _Logger = ZLogger.getLogger(TFileUploadServiceHandler.class);

	@Override
	public TFileResult uploadChunk(TChunkData chunkData) {
		Profiler.createThreadProfiler("uploadChunk", false);
		TFileResult ret;
		try {
			ret = TFileUploadModel.Instance.uploadChunk(chunkData);
		} finally {
			Profiler.closeThreadProfiler();
		}
		return ret;
	}


	@Override
	public TFileResult uploadChunkV2(TChunkData tcd, TZaloWorkerMessage tzwm) throws TException {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public TFileResult uploadFile(ByteBuffer bb) throws TException {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public TFileResult getFileInfo(long l) throws TException {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public TFileResult getDetailFileInfo(long l) throws TException {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public TFileResult uploadVideoChatViaHTTP(int i, String string, int i1) throws TException {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

}
