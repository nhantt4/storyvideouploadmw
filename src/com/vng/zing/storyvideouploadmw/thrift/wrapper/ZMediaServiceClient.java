package com.vng.zing.storyvideouploadmw.thrift.wrapper;

import com.vng.zing.common.ZCommonDef;
import com.vng.zing.common.ZErrorDef;
import com.vng.zing.configer.ZConfig;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.thriftpool.TClientPool;
import com.vng.zing.thriftpool.ZClientPoolUtil;
import com.vng.zing.zmcloud.thrift.ZMCMediaInfoResult;
import com.vng.zing.zmcloud.thrift.ZMCProcessOption;
import com.vng.zing.zmcloud.thrift.ZMCProcessResult;
import com.vng.zing.zmediaservice.thrift.ZMediaService;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.apache.thrift.TException;
import org.apache.thrift.transport.TTransportException;

/**
 *
 * @author tamnd2
 */
public class ZMediaServiceClient {

	private static final Class _ThisClass = ZMediaServiceClient.class;
	private static final Logger _Logger = ZLogger.getLogger(_ThisClass);
	private final String _name;
	private TClientPool.BizzConfig _bizzCfg;
	private ZMediaService.Client _aclient; //unused
	public String APP_ID;
	public String APP_KEY;
	public String OA_APP_ID;
	public String OA_APP_KEY;

	public ZMediaServiceClient(String name) {
		assert (name != null && !name.isEmpty());
		_name = name;
		_initialize();

	}

	public String getName() {
		return _name;
	}

	private void _initialize() {
		ZClientPoolUtil.SetDefaultPoolProp(_ThisClass /*clazzOfCfg*/,
				_name /*instName*/,
				null /*host*/,
				null /*auth*/,
				ZCommonDef.TClientTimeoutMilisecsDefault /*timeout*/,
				ZCommonDef.TClientNRetriesDefault /*nretry*/,
				ZCommonDef.TClientMaxRdAtimeDefault /*maxRdAtime*/,
				ZCommonDef.TClientMaxWrAtimeDefault /*maxWrAtime*/
		);
		ZClientPoolUtil.GetListPools(_ThisClass, _name, new ZMediaService.Client.Factory()); //auto create pools
		_bizzCfg = ZClientPoolUtil.GetBizzCfg(_ThisClass, _name);
		APP_ID = ZConfig.Instance.getString(_ThisClass, _name, "app_id", "");
		APP_KEY = ZConfig.Instance.getString(_ThisClass, _name, "app_key", "");
		OA_APP_ID = ZConfig.Instance.getString(_ThisClass, _name, "oa_app_id", "");
		OA_APP_KEY = ZConfig.Instance.getString(_ThisClass, _name, "oa_app_key", "");
	}

	private TClientPool<ZMediaService.Client> getClientPool() {
		return (TClientPool<ZMediaService.Client>) ZClientPoolUtil.GetPool(_ThisClass, _name);
	}

	private TClientPool.BizzConfig getBizzCfg() {
		return _bizzCfg;
	}

	///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	///error objects
	///
	///e1001: NO_CONNECTION
	public static final ZMCProcessResult ZMCProcessResult_NO_CONNECTION = new ZMCProcessResult(ZErrorDef.NO_CONNECTION);
	public static final ZMCMediaInfoResult ZMCMediaInfoResult_NO_CONNECTION = new ZMCMediaInfoResult(ZErrorDef.NO_CONNECTION);
	///e1002: BAD_CONNECTION
	public static final ZMCProcessResult ZMCProcessResult_BAD_CONNECTION = new ZMCProcessResult(ZErrorDef.BAD_CONNECTION);
	public static final ZMCMediaInfoResult ZMCMediaInfoResult_BAD_CONNECTION = new ZMCMediaInfoResult(ZErrorDef.BAD_CONNECTION);
	///e1003: BAD_REQUEST
	public static final ZMCProcessResult ZMCProcessResult_BAD_REQUEST = new ZMCProcessResult(ZErrorDef.BAD_REQUEST);
	public static final ZMCMediaInfoResult ZMCMediaInfoResult_BAD_REQUEST = new ZMCMediaInfoResult(ZErrorDef.BAD_REQUEST);

	///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	/// util functions
	///
	private static void Log(Priority level, Throwable t) {
		_Logger.log(level, null, t);
	}

	private static void Log(Priority level, Throwable t, int retry) {
		if (retry > 0) {
			String message = "Request's still failed at retry " + retry;
			_Logger.log(level, message, t);
		} else {
			_Logger.log(level, null, t);
		}
	}

	public ZMCProcessResult processForOA(ZMCProcessOption option) {
		TClientPool<ZMediaService.Client> pool = getClientPool();
		TClientPool.BizzConfig bCfg = getBizzCfg();
		for (int retry = 0; retry < bCfg.getNRetry(); ++retry) {
			ZMediaService.Client cli = pool.borrowClient();
			if (cli == null) {
				return ZMCProcessResult_NO_CONNECTION;
			}
			try {
				ZMCProcessResult result = cli.process(option, OA_APP_ID, OA_APP_KEY);
				pool.returnClient(cli);
				return result;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZMCProcessResult_BAD_REQUEST;
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZMCProcessResult_BAD_REQUEST;
			}
		}
		return ZMCProcessResult_BAD_CONNECTION;
	}

	public ZMCProcessResult process(ZMCProcessOption option) {
		TClientPool<ZMediaService.Client> pool = getClientPool();
		TClientPool.BizzConfig bCfg = getBizzCfg();
		for (int retry = 0; retry < bCfg.getNRetry(); ++retry) {
			ZMediaService.Client cli = pool.borrowClient();
			if (cli == null) {
				return ZMCProcessResult_NO_CONNECTION;
			}
			try {
				ZMCProcessResult result = cli.process(option, APP_ID, APP_KEY);
				pool.returnClient(cli);
				return result;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZMCProcessResult_BAD_REQUEST;
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZMCProcessResult_BAD_REQUEST;
			}
		}
		return ZMCProcessResult_BAD_CONNECTION;
	}

	public ZMCMediaInfoResult getMediaInfo(String mediaId) {
		TClientPool<ZMediaService.Client> pool = getClientPool();
		TClientPool.BizzConfig bCfg = getBizzCfg();
		for (int retry = 0; retry < bCfg.getNRetry(); ++retry) {
			ZMediaService.Client cli = pool.borrowClient();
			if (cli == null) {
				return ZMCMediaInfoResult_NO_CONNECTION;
			}
			try {
				ZMCMediaInfoResult result = cli.getMediaInfo(mediaId, APP_ID, APP_KEY);
				pool.returnClient(cli);
				return result;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZMCMediaInfoResult_BAD_REQUEST;
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZMCMediaInfoResult_BAD_REQUEST;
			}
		}
		return ZMCMediaInfoResult_BAD_CONNECTION;
	}
}
