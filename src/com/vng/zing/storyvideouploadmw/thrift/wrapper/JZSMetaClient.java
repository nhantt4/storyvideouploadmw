/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zing.storyvideouploadmw.thrift.wrapper;

import com.vng.zing.common.ZCommonDef;
import com.vng.zing.common.ZErrorDef;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.thriftpool.TClientPool;
import com.vng.zing.thriftpool.ZClientPoolUtil;
import com.vng.zing.zbfmeta.thrift.TValue;
import com.vng.zing.zbfmeta.thrift.TValueResult;
import com.vng.zing.zbfmeta.thrift.ZBFMetaService;
import com.vng.zing.zcommon.thrift.PutPolicy;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.apache.thrift.TException;
import org.apache.thrift.transport.TTransportException;

/**
 *
 * @author namlq2
 */
public class JZSMetaClient {

	private static final Class _ThisClass = JZSMetaClient.class;
	private static final Logger _Logger = ZLogger.getLogger(_ThisClass);
	private final String _name;
	private TClientPool.BizzConfig _bizzCfg;
	private ZBFMetaService.Client _aclient; //unused

	// Get errro code
	public static final TValueResult TValueResult_NO_CONNECTION = new TValueResult(ZErrorDef.NO_CONNECTION);
	public static final TValueResult TValueResult_BAD_REQUEST = new TValueResult(ZErrorDef.BAD_REQUEST);
	public static final TValueResult TValueResult_BAD_CONNECTION = new TValueResult(ZErrorDef.BAD_CONNECTION);

	public JZSMetaClient(String name) {
		assert (name != null && !name.isEmpty());
		_name = name;
		_initialize();

	}

	private void _initialize() {
		ZClientPoolUtil.SetDefaultPoolProp(_ThisClass //clazzOfCfg
				, _name //instName
				, null //host
				, null //auth
				, ZCommonDef.TClientTimeoutMilisecsDefault //timeout
				, ZCommonDef.TClientNRetriesDefault //nretry
				, ZCommonDef.TClientMaxRdAtimeDefault //maxRdAtime
				, ZCommonDef.TClientMaxWrAtimeDefault //maxWrAtime
		);
		ZClientPoolUtil.GetListPools(_ThisClass, _name, new ZBFMetaService.Client.Factory()); //auto create pools
		_bizzCfg = ZClientPoolUtil.GetBizzCfg(_ThisClass, _name);
	}

	private TClientPool<ZBFMetaService.Client> getClientPool() {
		return (TClientPool<ZBFMetaService.Client>) ZClientPoolUtil.GetPool(_ThisClass, _name);
	}

	private TClientPool.BizzConfig getBizzCfg() {
		return _bizzCfg;
	}

	private static void Log(Priority level, Throwable t) {
		_Logger.log(level, null, t);
	}

	private static void Log(Priority level, Throwable t, int retry) {
		if (retry > 0) {
			String message = "Request's still failed at retry " + retry;
			_Logger.log(level, message, t);
		} else {
			_Logger.log(level, null, t);
		}
	}

	public long put(long fileId, TValue value, PutPolicy pup) {
		TClientPool<ZBFMetaService.Client> pool = getClientPool();
		TClientPool.BizzConfig bCfg = getBizzCfg();
		for (int retry = 0; retry < bCfg.getNRetry(); ++retry) {
			ZBFMetaService.Client cli = pool.borrowClient();
			if (cli == null) {
				return (long) ZErrorDef.NO_CONNECTION;
			}
			try {
				long ret = cli.put(bCfg.getOpHandle(), fileId, value, pup);
				pool.returnClient(cli);
				return ret;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return (long) ZErrorDef.BAD_REQUEST;
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return (long) ZErrorDef.BAD_REQUEST;
			}
		}
		return (long) ZErrorDef.BAD_CONNECTION;
	}

	public TValueResult get(long chunkId) {
		TClientPool<ZBFMetaService.Client> pool = getClientPool();
		TClientPool.BizzConfig bCfg = getBizzCfg();
		for (int retry = 0; retry < bCfg.getNRetry(); ++retry) {
			ZBFMetaService.Client cli = pool.borrowClient();
			if (cli == null) {
				return TValueResult_NO_CONNECTION;
			}
			try {
				TValueResult ret = cli.get(bCfg.getOpHandle(), chunkId);
				pool.returnClient(cli);
				return ret;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return TValueResult_BAD_REQUEST;
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return TValueResult_BAD_REQUEST;
			}
		}
		return TValueResult_BAD_CONNECTION;
	}
}
