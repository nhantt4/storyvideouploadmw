/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zing.storyvideouploadmw.thrift.wrapper;

import com.vng.zing.common.ZCommonDef;
import com.vng.zing.common.ZErrorDef;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.thriftpool.TClientPool;
import com.vng.zing.thriftpool.ZClientPoolUtil;
import com.vng.zing.zmedia.thrift.TMediaInfoResult;
import com.vng.zing.zmedia.thrift.TProcessOption;
import com.vng.zing.zmedia.thrift.TProcessResult;
import com.vng.zing.zmedia.thrift.ZMediaProcessService;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.apache.thrift.TException;
import org.apache.thrift.transport.TTransportException;

/**
 *
 * @author datbt
 */
public class ZMediaProcessClient {
	private static final Class _ThisClass = ZMediaProcessClient.class;
	private static final Logger _Logger = ZLogger.getLogger(_ThisClass);
	private final String _name;
	private TClientPool.BizzConfig _bizzCfg;
	
	public ZMediaProcessClient(String name) {
		assert (name != null && !name.isEmpty());
		_name = name;
		_initialize();
	}
	
	private void _initialize() {
		ZClientPoolUtil.SetDefaultPoolProp(_ThisClass //clazzOfCfg
				, _name //instName
				, null //host
				, null //auth
				, ZCommonDef.TClientTimeoutMilisecsDefault //timeout
				, ZCommonDef.TClientNRetriesDefault //nretry
				, ZCommonDef.TClientMaxRdAtimeDefault //maxRdAtime
				, ZCommonDef.TClientMaxWrAtimeDefault //maxWrAtime
		);
		ZClientPoolUtil.GetListPools(_ThisClass, _name, new ZMediaProcessService.Client.Factory()); //auto create pools
		_bizzCfg = ZClientPoolUtil.GetBizzCfg(_ThisClass, _name);
	}
	
	private TClientPool<ZMediaProcessService.Client> getClientPool() {
		return (TClientPool<ZMediaProcessService.Client>) ZClientPoolUtil.GetPool(_ThisClass, _name);
	}

	private TClientPool.BizzConfig getBizzCfg() {
		return _bizzCfg;
	}

	private static void Log(Priority level, Throwable t) {
		_Logger.log(level, null, t);
	}

	private static void Log(Priority level, Throwable t, int retry) {
		if (retry > 0) {
			String message = "Request's still failed at retry " + retry;
			_Logger.log(level, message, t);
		} else {
			_Logger.log(level, null, t);
		}
	}

	public TProcessResult process(TProcessOption option) {
		TClientPool<ZMediaProcessService.Client> pool = getClientPool();
		TClientPool.BizzConfig bCfg = getBizzCfg();
		for (int retry = 0; retry < bCfg.getNRetry(); ++retry) {
			ZMediaProcessService.Client cli = pool.borrowClient();
			if (cli == null) {
				return new TProcessResult(ZErrorDef.NO_CONNECTION);
			}
			try {
				TProcessResult result = cli.process(option);
				pool.returnClient(cli);
				return result;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return new TProcessResult(ZErrorDef.BAD_REQUEST);
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return new TProcessResult(ZErrorDef.BAD_REQUEST);
			}
		}
		return new TProcessResult(ZErrorDef.BAD_CONNECTION);
	}

	public TMediaInfoResult getMediaInfo(long mediaId) {
		TClientPool<ZMediaProcessService.Client> pool = getClientPool();
		TClientPool.BizzConfig bCfg = getBizzCfg();
		for (int retry = 0; retry < bCfg.getNRetry(); ++retry) {
			ZMediaProcessService.Client cli = pool.borrowClient();
			if (cli == null) {
				return new TMediaInfoResult(ZErrorDef.NO_CONNECTION);
			}
			try {
				TMediaInfoResult result = cli.getMediaInfo(mediaId);
				pool.returnClient(cli);
				return result;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return new TMediaInfoResult(ZErrorDef.BAD_REQUEST);
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return new TMediaInfoResult(ZErrorDef.BAD_REQUEST);
			}
		}
		return new TMediaInfoResult(ZErrorDef.BAD_CONNECTION);
	}
}
