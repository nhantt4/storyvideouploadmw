/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zing.storyvideouploadmw.thrift.wrapper;

import com.vng.zing.common.ZCommonDef;
import com.vng.zing.common.ZErrorDef;
import com.vng.zing.common.ZErrorHelper;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.thriftpool.TClientPool;
import com.vng.zing.thriftpool.ZClientPoolUtil;
import com.vng.zing.zapfileuploadmw.thrift.TChunkData;
import com.vng.zing.zapfileuploadmw.thrift.TFileResult;
import com.vng.zing.zapfileuploadmw.thrift.ZAPFileUploadMWService;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.apache.thrift.TException;
import org.apache.thrift.transport.TTransportException;

/**
 *
 * @author namnq
 */
public class ZAPFileUploadMWClient {

	private static final Class _ThisClass = ZAPFileUploadMWClient.class;
	private static final Logger _Logger = ZLogger.getLogger(_ThisClass);
	private final String _name;
	private TClientPool.BizzConfig _bizzCfg;
	private ZAPFileUploadMWService.Client _aclient; //unused

	public ZAPFileUploadMWClient(String name) {
		assert (name != null && !name.isEmpty());
		_name = name;
		_initialize();

	}

	private void _initialize() {
		ZClientPoolUtil.SetDefaultPoolProp(_ThisClass //clazzOfCfg
				, _name //instName
				, null //host
				, null //auth
				, ZCommonDef.TClientTimeoutMilisecsDefault //timeout
				, ZCommonDef.TClientNRetriesDefault //nretry
				, ZCommonDef.TClientMaxRdAtimeDefault //maxRdAtime
				, ZCommonDef.TClientMaxWrAtimeDefault //maxWrAtime
		);
		ZClientPoolUtil.GetListPools(_ThisClass, _name, new ZAPFileUploadMWService.Client.Factory()); //auto create pools
		_bizzCfg = ZClientPoolUtil.GetBizzCfg(_ThisClass, _name);
	}

	private TClientPool<ZAPFileUploadMWService.Client> getClientPool() {
		return (TClientPool<ZAPFileUploadMWService.Client>) ZClientPoolUtil.GetPool(_ThisClass, _name);
	}

	private TClientPool.BizzConfig getBizzCfg() {
		return _bizzCfg;
	}
	///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	///error objects
	///
	///e1001: NO_CONNECTION

	///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	/// util functions
	///
	private static void Log(Priority level, Throwable t) {
		_Logger.log(level, null, t);
	}

	private static void Log(Priority level, Throwable t, int retry) {
		if (retry > 0) {
			String message = "Request's still failed at retry " + retry;
			_Logger.log(level, message, t);
		} else {
			_Logger.log(level, null, t);
		}
	}

	public static List<List<Long/*KType*/>> SplitListKeys(List<Long/*KType*/> listKeys, int mkeysAtime) {
		if (listKeys == null) {
			return null;
		}
		//correct mkeysAtime
		if (mkeysAtime < 1) {
			mkeysAtime = 1;
		}
		List<List<Long/*KType*/>> ret = new LinkedList<List<Long/*KType*/>>();
		int size = listKeys.size();
		if (size <= mkeysAtime) {
			ret.add(listKeys);
			return ret;
		}
		int nsubList = size / mkeysAtime;
		for (int i = 0; i < nsubList; ++i) {
			int startIndex = i * mkeysAtime;
			ret.add(listKeys.subList(startIndex, startIndex + mkeysAtime));
		}
		int lastListSz = size % mkeysAtime;
		if (lastListSz > 0) {
			ret.add(listKeys.subList(size - lastListSz, size));
		}
		return ret;
	}

	public static int MergeError(int out, int error) {
		if (ZErrorHelper.isFail(error)) {
			out = error;
		}
		return out;
	}

	public static Map<Long/*KType*/, Integer> MergeErrorMap(Map<Long/*KType*/, Integer> out, Map<Long/*KType*/, Integer> errorMap) {
		if (errorMap != null && !errorMap.isEmpty()) {
			if (out == null) {
				out = new HashMap<Long/*KType*/, Integer>();
			}
			out.putAll(errorMap);
		}
		return out;
	}

	///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	///spec for ZAPFileUploadMW
	///
	//..
	public TFileResult uploadChunk(TChunkData chunkData) {
		TClientPool<ZAPFileUploadMWService.Client> pool = getClientPool();
		TClientPool.BizzConfig bCfg = getBizzCfg();
		for (int retry = 0; retry < bCfg.getNRetry(); ++retry) {
			ZAPFileUploadMWService.Client cli = pool.borrowClient();
			if (cli == null) {
				return new TFileResult(ZErrorDef.NO_CONNECTION);
			}
			try {
				TFileResult ret = cli.uploadChunk(chunkData);
				pool.returnClient(cli);
				return ret;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return new TFileResult(ZErrorDef.BAD_REQUEST);
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return new TFileResult(ZErrorDef.BAD_REQUEST);
			}
		}
		return new TFileResult(ZErrorDef.BAD_CONNECTION);
	}

	public TFileResult uploadVideoChatViaHTTP(int userId, String uploadUrl, int totalSize) {
		TClientPool<ZAPFileUploadMWService.Client> pool = getClientPool();
		TClientPool.BizzConfig bCfg = getBizzCfg();
		for (int retry = 0; retry < bCfg.getNRetry(); ++retry) {
			ZAPFileUploadMWService.Client cli = pool.borrowClient();
			if (cli == null) {
				return new TFileResult(ZErrorDef.NO_CONNECTION);
			}
			try {
				TFileResult ret = cli.uploadVideoChatViaHTTP(userId, uploadUrl, totalSize);
				pool.returnClient(cli);
				return ret;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return new TFileResult(ZErrorDef.BAD_REQUEST);
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return new TFileResult(ZErrorDef.BAD_REQUEST);
			}
		}
		return new TFileResult(ZErrorDef.BAD_CONNECTION);
	}
}
