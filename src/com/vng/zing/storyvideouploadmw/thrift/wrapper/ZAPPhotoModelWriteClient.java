package com.vng.zing.storyvideouploadmw.thrift.wrapper;

import com.vng.zing.common.ZCommonDef;
import com.vng.zing.common.ZErrorDef;
import com.vng.zing.common.ZErrorHelper;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.thriftpool.TClientPool;
import com.vng.zing.thriftpool.ZClientPoolUtil;
import com.vng.zing.zapalbummeta.thrift.TValue;
import com.vng.zing.zapphotometa.thrift.TValueResult;
import com.vng.zing.zapphotomw.thrift.TMapPhotoUrlResult;
import com.vng.zing.zapphotomw.thrift.TPutPhotoData;
import com.vng.zing.zapphotomw.thrift.TPutPhotoResult;
import com.vng.zing.zapphotomw.thrift.ZAPPhotoModelWriteService;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.apache.thrift.TException;
import org.apache.thrift.transport.TTransportException;

/**
 *
 * @author namnq
 */
public class ZAPPhotoModelWriteClient {

	private static final Class _ThisClass = ZAPPhotoModelWriteClient.class;
	private static final Logger _Logger = ZLogger.getLogger(_ThisClass);
	private final String _name;
	private TClientPool.BizzConfig _bizzCfg;
	private ZAPPhotoModelWriteService.Client _aclient; //unused

	public ZAPPhotoModelWriteClient(String name) {
		assert (name != null && !name.isEmpty());
		_name = name;
		_initialize();

	}

	private void _initialize() {
		ZClientPoolUtil.SetDefaultPoolProp(_ThisClass //clazzOfCfg
				,
				 _name //instName
				,
				 null //host
				,
				 null //auth
				,
				 ZCommonDef.TClientTimeoutMilisecsDefault //timeout
				,
				 ZCommonDef.TClientNRetriesDefault //nretry
				,
				 ZCommonDef.TClientMaxRdAtimeDefault //maxRdAtime
				,
				 ZCommonDef.TClientMaxWrAtimeDefault //maxWrAtime
		);
		ZClientPoolUtil.GetListPools(_ThisClass, _name, new ZAPPhotoModelWriteService.Client.Factory()); //auto create pools
		_bizzCfg = ZClientPoolUtil.GetBizzCfg(_ThisClass, _name);
	}

	private TClientPool<ZAPPhotoModelWriteService.Client> getClientPool() {
		return (TClientPool<ZAPPhotoModelWriteService.Client>) ZClientPoolUtil.GetPool(_ThisClass, _name);
	}

	private TClientPool.BizzConfig getBizzCfg() {
		return _bizzCfg;
	}
	///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	///error objects
	///
	///e1001: NO_CONNECTION
	public static final TMapPhotoUrlResult TMapPhotoUrlResult_NO_CONNECTION = new TMapPhotoUrlResult(ZErrorDef.NO_CONNECTION);
	public static final TPutPhotoResult TPutPhotoResult_NO_CONNECTION = new TPutPhotoResult(ZErrorDef.NO_CONNECTION);
	public static final TValueResult TValueResult_Photo_NO_CONNECTION = new TValueResult(ZErrorDef.NO_CONNECTION);
//	public static final TUserStatusResult TUserStatusResult_NO_CONNECTION = new TUserStatusResult(ZErrorDef.NO_CONNECTION);
////	///e1002: BAD_CONNECTION
	public static final TPutPhotoResult TPutPhotoResult_BAD_CONNECTION = new TPutPhotoResult(ZErrorDef.BAD_CONNECTION);
	public static final TValueResult TValueResult_Photo_BAD_CONNECTION = new TValueResult(ZErrorDef.BAD_CONNECTION);
	public static final TMapPhotoUrlResult TMapPhotoUrlResult_BAD_CONNECTION = new TMapPhotoUrlResult(ZErrorDef.BAD_CONNECTION);
//	public static final TUserStatusResult TUserStatusResult_BAD_CONNECTION = new TUserStatusResult(ZErrorDef.BAD_CONNECTION);
////	///e1003: BAD_REQUEST
	public static final TPutPhotoResult TPutPhotoResult_BAD_REQUEST = new TPutPhotoResult(ZErrorDef.BAD_REQUEST);
	public static final TValueResult TValueResult_Photo_BAD_REQUEST = new TValueResult(ZErrorDef.BAD_REQUEST);
	public static final TMapPhotoUrlResult TMapPhotoUrlResult_BAD_REQUEST = new TMapPhotoUrlResult(ZErrorDef.BAD_REQUEST);
//	public static final TUserStatusResult TUserStatusResult_BAD_REQUEST = new TUserStatusResult(ZErrorDef.BAD_REQUEST);

	///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	/// util functions
	///
	private static void Log(Priority level, Throwable t) {
		_Logger.log(level, null, t);
	}

	private static void Log(Priority level, Throwable t, int retry) {
		if (retry > 0) {
			String message = "Request's still failed at retry " + retry;
			_Logger.log(level, message, t);
		} else {
			_Logger.log(level, null, t);
		}
	}

	public static List<List<Integer/*
             * KType
			 */>> SplitListKeys(List<Integer/*
                     * KType
					 */> listKeys, int mkeysAtime) {
		if (listKeys == null) {
			return null;
		}
		//correct mkeysAtime
		if (mkeysAtime < 1) {
			mkeysAtime = 1;
		}
		List<List<Integer/*
                 * KType
				 */>> ret = new LinkedList<List<Integer/*
                 * KType
				 */>>();
		int size = listKeys.size();
		if (size <= mkeysAtime) {
			ret.add(listKeys);
			return ret;
		}
		int nsubList = size / mkeysAtime;
		for (int i = 0; i < nsubList; ++i) {
			int startIndex = i * mkeysAtime;
			ret.add(listKeys.subList(startIndex, startIndex + mkeysAtime));
		}
		int lastListSz = size % mkeysAtime;
		if (lastListSz > 0) {
			ret.add(listKeys.subList(size - lastListSz, size));
		}
		return ret;
	}

	public static int MergeError(int out, int error) {
		if (ZErrorHelper.isFail(error)) {
			out = error;
		}
		return out;
	}

	public static Map<Integer/*
             * KType
			 */, String/*
             * VType
			 */> MergeDataMap(Map<Integer/*
                     * KType
					 */, String/*
                     * VType
					 */> out, Map<Integer/*
                     * KType
					 */, String/*
                     * VType
					 */> dataMap) {
		if (dataMap != null && !dataMap.isEmpty()) {
			if (out == null) {
				out = new HashMap<Integer/*
                         * KType
						 */, String/*
                         * VType
						 */>();
			}
			out.putAll(dataMap);
		}
		return out;
	}

	public static Map<Integer/*
             * KType
			 */, Integer> MergeErrorMap(Map<Integer/*
                     * KType
					 */, Integer> out, Map<Integer/*
                     * KType
					 */, Integer> errorMap) {
		if (errorMap != null && !errorMap.isEmpty()) {
			if (out == null) {
				out = new HashMap<Integer/*
                         * KType
						 */, Integer>();
			}
			out.putAll(errorMap);
		}
		return out;
	}

	///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	/// error objects: spec ZPRawApiPageCommunication
	///
	///e1001: NO_CONNECTION
	//..
	///e1002: BAD_CONNECTION
	//..
	///e1003: BAD_REQUEST
	//..
	///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	/// util functions: spec ZPRawApiPageCommunication
	///
	//..
	///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	///common methods
	///
	public TPutPhotoResult putPhoto(TPutPhotoData photo) {
		TClientPool<ZAPPhotoModelWriteService.Client> pool = getClientPool();
		for (int retry = 0; retry < pool.getNRetry(); ++retry) {
			ZAPPhotoModelWriteService.Client cli = pool.borrowClient();
			if (cli == null) {
				return TPutPhotoResult_NO_CONNECTION;
			}
			try {
				TPutPhotoResult ret = cli.putPhoto(photo);
				pool.returnClient(cli);
				return ret;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return TPutPhotoResult_BAD_REQUEST;
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return TPutPhotoResult_BAD_REQUEST;
			}
		}
		return TPutPhotoResult_BAD_CONNECTION;
	}

	public long putPhotoMeta(Long photoId, com.vng.zing.zapphotometa.thrift.TValue photo) {
		TClientPool<ZAPPhotoModelWriteService.Client> pool = getClientPool();
		for (int retry = 0; retry < pool.getNRetry(); ++retry) {
			ZAPPhotoModelWriteService.Client cli = pool.borrowClient();
			if (cli == null) {
				return ZErrorDef.NO_CONNECTION;
			}
			try {
				long ret = cli.putPhotoMeta(photoId, photo);
				pool.returnClient(cli);
				return ret;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZErrorDef.BAD_REQUEST;
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZErrorDef.BAD_REQUEST;
			}
		}
		return ZErrorDef.BAD_CONNECTION;
	}

	public long nextPhotoId() {
		TClientPool<ZAPPhotoModelWriteService.Client> pool = getClientPool();
		for (int retry = 0; retry < pool.getNRetry(); ++retry) {
			ZAPPhotoModelWriteService.Client cli = pool.borrowClient();
			if (cli == null) {
				return ZErrorDef.NO_CONNECTION;
			}
			try {
				long ret = cli.nextPhotoId();
				pool.returnClient(cli);
				return ret;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZErrorDef.BAD_REQUEST;
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZErrorDef.BAD_REQUEST;
			}
		}
		return ZErrorDef.BAD_CONNECTION;
	}

	public long putPhotoMetaNew(Long photoId, com.vng.zing.zapphotometa.thrift.TValue photo) {
		TClientPool<ZAPPhotoModelWriteService.Client> pool = getClientPool();
		for (int retry = 0; retry < pool.getNRetry(); ++retry) {
			ZAPPhotoModelWriteService.Client cli = pool.borrowClient();
			if (cli == null) {
				return ZErrorDef.NO_CONNECTION;
			}
			try {
				long ret = cli.putPhotoMetaNew(photoId, photo);
				pool.returnClient(cli);
				return ret;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZErrorDef.BAD_REQUEST;
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZErrorDef.BAD_REQUEST;
			}
		}
		return ZErrorDef.BAD_CONNECTION;
	}

	public long updatePhotoMetaField(Long photoId, com.vng.zing.zapphotometa.thrift.TValue photo) {
		TClientPool<ZAPPhotoModelWriteService.Client> pool = getClientPool();
		for (int retry = 0; retry < pool.getNRetry(); ++retry) {
			ZAPPhotoModelWriteService.Client cli = pool.borrowClient();
			if (cli == null) {
				return ZErrorDef.NO_CONNECTION;
			}
			try {
				long ret = cli.updatePhotoMetaField(photoId, photo);
				pool.returnClient(cli);
				return ret;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZErrorDef.BAD_REQUEST;
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZErrorDef.BAD_REQUEST;
			}
		}
		return ZErrorDef.BAD_CONNECTION;
	}

	public TValueResult getPhotoMeta(Long photoId) {
		TClientPool<ZAPPhotoModelWriteService.Client> pool = getClientPool();
		for (int retry = 0; retry < pool.getNRetry(); ++retry) {
			ZAPPhotoModelWriteService.Client cli = pool.borrowClient();
			if (cli == null) {
				return TValueResult_Photo_NO_CONNECTION;
			}
			try {
				TValueResult ret = cli.getPhotoMeta(photoId);
				pool.returnClient(cli);
				return ret;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return TValueResult_Photo_BAD_REQUEST;
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return TValueResult_Photo_BAD_REQUEST;
			}
		}
		return TValueResult_Photo_BAD_CONNECTION;
	}

	public long putAlbumPhoto(Long albumId, Long photoId) {
		TClientPool<ZAPPhotoModelWriteService.Client> pool = getClientPool();
		for (int retry = 0; retry < pool.getNRetry(); ++retry) {
			ZAPPhotoModelWriteService.Client cli = pool.borrowClient();
			if (cli == null) {
				return ZErrorDef.NO_CONNECTION;
			}
			try {
				long ret = cli.putAlbumPhoto(albumId, photoId);
				pool.returnClient(cli);
				return ret;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZErrorDef.BAD_REQUEST;
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZErrorDef.BAD_REQUEST;
			}
		}
		return ZErrorDef.BAD_CONNECTION;
	}

	public long putAlbumMeta(Long albumId, TValue album) {
		TClientPool<ZAPPhotoModelWriteService.Client> pool = getClientPool();
		for (int retry = 0; retry < pool.getNRetry(); ++retry) {
			ZAPPhotoModelWriteService.Client cli = pool.borrowClient();
			if (cli == null) {
				return ZErrorDef.NO_CONNECTION;
			}
			try {
				long ret = cli.putAlbumMeta(albumId, album);
				pool.returnClient(cli);
				return ret;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZErrorDef.BAD_REQUEST;
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZErrorDef.BAD_REQUEST;
			}
		}
		return ZErrorDef.BAD_CONNECTION;
	}

	public long updateAlbumMetaField(Long albumId, TValue album) {
		TClientPool<ZAPPhotoModelWriteService.Client> pool = getClientPool();
		for (int retry = 0; retry < pool.getNRetry(); ++retry) {
			ZAPPhotoModelWriteService.Client cli = pool.borrowClient();
			if (cli == null) {
				return ZErrorDef.NO_CONNECTION;
			}
			try {
				long ret = cli.updateAlbumMetaField(albumId, album);
				pool.returnClient(cli);
				return ret;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZErrorDef.BAD_REQUEST;
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZErrorDef.BAD_REQUEST;
			}
		}
		return ZErrorDef.BAD_CONNECTION;
	}

	public long removeAlbum(Long albumId) {
		TClientPool<ZAPPhotoModelWriteService.Client> pool = getClientPool();
		for (int retry = 0; retry < pool.getNRetry(); ++retry) {
			ZAPPhotoModelWriteService.Client cli = pool.borrowClient();
			if (cli == null) {
				return ZErrorDef.NO_CONNECTION;
			}
			try {
				long ret = cli.removeAlbum(albumId);
				pool.returnClient(cli);
				return ret;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZErrorDef.BAD_REQUEST;
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZErrorDef.BAD_REQUEST;
			}
		}
		return ZErrorDef.BAD_CONNECTION;
	}

	public long removePhoto(Long photoId) {
		TClientPool<ZAPPhotoModelWriteService.Client> pool = getClientPool();
		for (int retry = 0; retry < pool.getNRetry(); ++retry) {
			ZAPPhotoModelWriteService.Client cli = pool.borrowClient();
			if (cli == null) {
				return ZErrorDef.NO_CONNECTION;
			}
			try {
				long ret = cli.removePhoto(photoId);
				pool.returnClient(cli);
				return ret;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZErrorDef.BAD_REQUEST;
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return ZErrorDef.BAD_REQUEST;
			}
		}
		return ZErrorDef.BAD_CONNECTION;
	}

	public TMapPhotoUrlResult multiGetPhotoUrl(List<Long> photoIds, List<Short> lsSize) {
		TClientPool<ZAPPhotoModelWriteService.Client> pool = getClientPool();
		for (int retry = 0; retry < pool.getNRetry(); ++retry) {
			ZAPPhotoModelWriteService.Client cli = pool.borrowClient();
			if (cli == null) {
				return TMapPhotoUrlResult_NO_CONNECTION;
			}
			try {
				TMapPhotoUrlResult ret = cli.multiGetPhotoUrl(photoIds, lsSize);
				pool.returnClient(cli);
				return ret;
			} catch (TTransportException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				continue;
			} catch (TException ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return TMapPhotoUrlResult_BAD_REQUEST;
			} catch (Exception ex) {
				pool.invalidateClient(cli, ex);
				Log(Priority.ERROR, ex, retry);
				return TMapPhotoUrlResult_BAD_REQUEST;
			}
		}
		return TMapPhotoUrlResult_BAD_CONNECTION;
	}
}
