/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */
package com.vng.zing.storyvideouploadmw.servers;

import com.vng.zing.storyvideouploadmw.handlers.TFileUploadServiceHandler;
import com.vng.zing.thriftserver.ThriftServers;
import com.vng.zing.zapfileuploadmw.thrift.ZAPFileUploadMWService;

/**
 *
 * @author namnq
 */
public class TServers {

	public boolean setupAndStart() {
		ThriftServers servers = new ThriftServers("zapfileuploadmw"); // TODO Sửa config !!!
		ZAPFileUploadMWService.Processor processor = new ZAPFileUploadMWService.Processor(new TFileUploadServiceHandler());
		servers.setup(processor);
		return servers.start();
	}
}
